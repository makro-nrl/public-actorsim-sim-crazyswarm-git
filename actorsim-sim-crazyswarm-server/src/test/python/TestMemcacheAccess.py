import unittest

from memcache import Client


class TestMemcacheAccess(unittest.TestCase):

    def setUp(self):
        servers = ["127.0.0.1:11211"]
        self.mc = Client(servers, debug=1)

    def tearDown(self):
        self.mc.flush_all()
        self.mc.disconnect_all()

    def test_memcache_write(self):
        self.check_setget("my_key", 10)

    def test_memcache_read(self):
        value = self.mc.get("my_key")
        self.assertEqual(value, 10)

    def check_setget(self, key, val, noreply=False):
        self.mc.set(key, val, noreply=noreply)
        newval = self.mc.get(key)
        self.assertEqual(newval, val)

