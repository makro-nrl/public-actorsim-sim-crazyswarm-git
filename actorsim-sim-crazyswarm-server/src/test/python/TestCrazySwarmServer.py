import time
import unittest

from server import Server, Command


class TestCrazySwarmServer(unittest.TestCase):
    duration_in_millis = 1000

    def setUp(self):
        self.memcache = Server.init_memcache_client()

    def tearDown(self):
        self.memcache.flush_all()
        self.memcache.disconnect_all()

    def test_square(self):
        commands = [
            Command.start(),
            Command.takeoff(),
            Command.move([0, 1, 1], self.duration_in_millis),
            Command.move([1, 1, 1], self.duration_in_millis),
            Command.move([1, 0, 1], self.duration_in_millis),
            Command.move([0, 0, 1], self.duration_in_millis),
            Command.land(),
            Command.stop()
        ]

        self.run_commands(commands)

    def test_two_vehicles(self):
        num_vehicles = 2
        v1 = 1
        v2 = 2
        commands = [
            Command.start(num_vehicles),
            Command.takeoff(),
            Command.move([0, 1, 1], self.duration_in_millis, v1),
            Command.move([2, 1, 1], self.duration_in_millis, v2),

            Command.move([1, 1, 1], self.duration_in_millis, v1),
            Command.move([3, 1, 1], self.duration_in_millis, v2),

            Command.move([1, 0, 1], self.duration_in_millis, v1),
            Command.move([3, 0, 1], self.duration_in_millis, v2),

            Command.move([0, 0, 1], self.duration_in_millis, v1),
            Command.move([0, 0, 1], self.duration_in_millis, v2),

            Command.land(),
            Command.stop()
        ]

        self.run_commands(commands)

    def test_three_vehicles(self):
        num_vehicles = 3
        v1 = 1
        v2 = 2
        v3 = 3
        commands = [
            Command.start(num_vehicles),
            Command.takeoff(),

            Command.move([0, 1, 1], self.duration_in_millis, v1),
            Command.move([2, 1, 1], self.duration_in_millis, v2),
            Command.move([4, 1, 1], self.duration_in_millis, v3),

            Command.move([1, 1, 1], self.duration_in_millis, v1),
            Command.move([3, 1, 1], self.duration_in_millis, v2),
            Command.move([5, 1, 1], self.duration_in_millis, v3),

            Command.move([1, 0, 1], self.duration_in_millis, v1),
            Command.move([3, 0, 1], self.duration_in_millis, v2),
            Command.move([5, 0, 1], self.duration_in_millis, v3),

            Command.move([0, 0, 1], self.duration_in_millis, v1),
            Command.move([0, 0, 1], self.duration_in_millis, v2),
            Command.move([0, 0, 1], self.duration_in_millis, v3),

            Command.land(),
            Command.stop()
        ]

        self.run_commands(commands)



    def run_commands(self, commands):
        next_id = 0

        for command in commands:
            command.set_id(next_id)
            next_id += 1
            print("")
            print("Sending command: {}".format(command))
            json_command = command.dumps()
            self.memcache.set(Server.COMMAND_KEY, json_command)
            result = self.wait_for_result(command)  # type: Command
            duration_in_seconds = self.duration_in_millis / 1000
            time.sleep(duration_in_seconds + 0.2)
            assert result.is_success()



    def wait_for_result(self, command):
        print "Waiting for result of command: " + str(command)
        while True:
            json_result = self.memcache.get(Server.RESULT_KEY)
            if json_result is not None:
                result = Command.loads(json_result)   # type: Command
                if result.id == command.id:
                    print "Result received: " + str(result)
                    Server.delete_result_key(self.memcache)
                    return result
            time.sleep(1)


if __name__ == "__main__":
    unittest.main()
