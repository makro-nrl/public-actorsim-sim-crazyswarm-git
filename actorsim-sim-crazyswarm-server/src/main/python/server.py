from __future__ import print_function

import json
import matplotlib.pyplot
import time
import traceback

from enum import Enum
from memcache import Client

import pycrazyswarm.visualizer.visMatplotlib
from pycrazyswarm import Crazyswarm


class Result(str, Enum):
    SUCCESS = 'SUCCESS'
    UNKNOWN = 'UNKNOWN'
    FAIL = 'FAIL'


class Action(str, Enum):
    STOP = 'STOP'
    START = 'START'
    TAKEOFF = 'TAKEOFF'
    MOVE = 'MOVE'
    LAND = 'LAND'


class Command:
    def __init__(self, action):  # type (Action) -> Command
        self.id = -1
        self.vehicle_id = 1
        self.action = action
        self.num_vehicles = 1
        self.waypoint = None
        self.duration_in_millis = 0
        self.sleep_after_command_in_millis = 0.1
        self.result = Result.UNKNOWN

    def __str__(self):
        value = str(self.id) + ":" + str(self.action) + "(" + str(self.vehicle_id) + ")"
        if self.waypoint is not None:
            value += str(self.waypoint)

        value += " durMS:" + str(self.duration_in_millis)
        value += " pauseMS:" + str(self.sleep_after_command_in_millis)

        value += " result:"
        if self.is_success():
            value += "SUCCESS"
        elif self.is_fail():
            value += "FAIL"
        else:
            value += "UNKNOWN"

        return value

    def dumps(self):
        return json.dumps(self, cls=self.CommandEncoder)

    class CommandEncoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, Command):
                dct = o.__dict__
                dct["__Command__"] = True
                return dct
            return json.JSONEncoder.default(self, o)

    @staticmethod
    def loads(s):
        return json.loads(s, object_hook=Command.as_command)

    @staticmethod
    def as_command(dct):
        if '__Command__' in dct:
            command = Command(dct['action'])
            command.set_id(dct['id'])
            command.vehicle_id = dct['vehicle_id']
            command.num_vehicles = dct['num_vehicles']
            if command.action == Action.MOVE:
                command.waypoint = dct['waypoint']
                command.duration_in_millis = dct['duration_in_millis']
            command.sleep_after_command_in_millis = dct['sleep_after_command_in_millis']
            command.set_result(dct['result'])
            return command
        return dct

    def duration_in_seconds(self):
        return self.duration_in_millis / 1000;

    def exec_duration_in_seconds(self):
        return self.duration_in_seconds() \
               + (self.sleep_after_command_in_millis / 1000);

    def vehicle(self, vehicle):
        self.vehicle_id = vehicle
        return self

    def to(self, waypoint, duration_in_millis):
        self.waypoint = waypoint
        self.duration_in_millis = duration_in_millis
        return self

    def set_id(self, id_in):
        self.id = id_in

    def set_result(self, result):
        self.result = result

    def is_success(self):
        return self.result == Result.SUCCESS

    def is_fail(self):
        return self.result == Result.FAIL

    def is_start(self):
        return self.action == Action.START

    def is_stop(self):
        return self.action == Action.STOP

    def is_takeoff(self):
        return self.action == Action.TAKEOFF

    def is_land(self):
        return self.action == Action.LAND

    def is_move(self):
        return self.action == Action.MOVE

    @staticmethod
    def start(num_vehicles = 1):
        command = Command(Action.START)
        command.num_vehicles = num_vehicles
        return command

    @staticmethod
    def stop():
        return Command(Action.STOP)

    @staticmethod
    def takeoff():
        return Command(Action.TAKEOFF)

    @staticmethod
    def land():
        return Command(Action.LAND)

    @staticmethod
    def move(location, duration_in_millis, vehicle_id=1):
        return Command(Action.MOVE).to(location, duration_in_millis).vehicle(vehicle_id)


class Server:
    COMMAND_KEY = "crazyswarm_command"
    RESULT_KEY = "crazyswarm_result"
    SLEEP_BETWEEN_COMMANDS = 0.1

    def __init__(self):
        self.keep_running = True
        self.mission_running = False
        self.vehicles = None
        self.memcache = Server.init_memcache_client()

    @staticmethod
    def select_best_matplotlib_backend():
        """
        My attempts to get the server to show the execution window
        from inside of IntelliJ have failed.

        It seems related to the default matplotlib backend, but I was
        unable to get the backend to switch to Qt5 inside of IntelliJ.

        Here are some notes in case we ever decide to pick up this thread:
        - https://intellij-support.jetbrains.com/hc/en-us/community/posts/360004382380-Matplotlib-doesn-t-show-plots-in-new-window
        - https://www.jetbrains.com/help/idea/matplotlib-tutorial.html#sample_code
        - https://stackoverflow.com/questions/3580027/how-do-you-determine-which-backend-is-being-used-by-matplotlib#3580047
        - https://intellij-support.jetbrains.com/hc/en-us/community/posts/115000551170-PyQt4-and-PyQt5-collisions-in-PyCharm-2017-2-1-when-debugging-QGIS-application
        - https://stackoverflow.com/questions/3285193/how-to-change-backends-in-matplotlib-python
        """
        print("Selecting the best matplotlib backend")
        gui_env = ['Qt5Agg','Qt4Agg','TKAgg','GTKAgg','WXAgg']
        for gui in gui_env:
            try:
                print("  trying backend:", gui)
                matplotlib.use(gui,warn=False, force=True)
                from matplotlib import pyplot as plt
                break
            except:
                continue
        print("Using:",matplotlib.get_backend())
        plt.show()
        time.sleep(3)


    def run(self):
        print_start = True
        while self.keep_running:
            try:
                if print_start:
                    print("Waiting for command...")
                    print_start = False

                # noinspection PyTypeChecker
                command = None  # type: Command
                json_command = self.read_command()
                if json_command is not None:
                    command = Command.loads(json_command)
                if command is None:
                    time.sleep(self.SLEEP_BETWEEN_COMMANDS)
                else:
                    Server.delete_command_key(self.memcache)
                    self.run_command(command)
                    self.send_result(command)

                    print_start = True

                if not self.keep_running:
                    break
            except Exception as e:
                print("exception occurred:{}".format(e))
                print("{}".format(traceback.format_exc()))

    def is_mission_running(self):
        return self.mission_running

    def read_command(self):
        return self.memcache.get(Server.COMMAND_KEY)

    def run_command(self, command):  # type: (Command) -> None
        print("Running command: {}".format(command))
        if command.is_start():
            self.start_new_mission(command)
        elif command.is_stop():
            self.stop_mission(command)
        elif command.is_takeoff():
            self.command_takeoff(command)
        elif command.is_land():
            self.command_land(command)
        elif command.is_move():
            self.command_move(command)

    def send_result(self, command):
        print("Sending result: {}".format(command))
        json_result = command.dumps()
        self.memcache.set(Server.RESULT_KEY, json_result)

    # noinspection PyAttributeOutsideInit
    def start_new_mission(self, command):
        print("  Starting a new mission")
        yaml = self.build_yaml(command)
        print("Using yaml: \n" + yaml)
        self.swarm = Crazyswarm(crazyflies_yaml=yaml, args=["--sim"])
        self.timeHelper = self.swarm.timeHelper
        self.vehicles = self.swarm.allcfs
        # if (command.num_vehicles > 1):
        #     self.vehicles[2].setLEDcolor(1, 0, 0)
        # if (command.num_vehicles > 2):
        #     self.vehicles[3].setLEDcolor(0, 0, 1)
        self.mission_running = True
        command.set_result(Result.SUCCESS)

    def build_yaml(self, command):

        # this yaml code was adapted from the graphVisualization.py code
        crazyflies_yaml = """
        crazyflies:
        - id: 1
          channel: 110
          initialPosition: [0.0, 0.0, 0.0]
        """
        if command.num_vehicles >= 2:
            crazyflies_yaml += """
        - id: 2
          channel: 120
          initialPosition: [0.0, 0.0, 0.0]
            """
        if command.num_vehicles >= 3:
            crazyflies_yaml += """
        - id: 3
          channel: 100
          initialPosition: [0.0, 0.0, 0.0]
            """
        # - id: 4
        #   channel: 110
        #   initialPosition: [0.5, 0.0, 0.0]
        # - id: 5
        #   channel: 120
        #   initialPosition: [1.0, 0.0, 0.0]

        return crazyflies_yaml

    def stop_mission(self, command):
        print("  Stopping the mission")

        # noinspection PyUnresolvedReferences
        visualizer = self.timeHelper.visualizer  # type: pycrazyswarm.visualizer.visMatplotlib.VisMatplotlib()
        matplotlib.pyplot.close(visualizer.fig)
        self.mission_running = False
        command.set_result(Result.SUCCESS)

    def command_takeoff(self, command):
        self.vehicles.takeoff(targetHeight=1.0, duration=command.duration_in_seconds())
        self.timeHelper.sleep(command.exec_duration_in_seconds())
        command.set_result(Result.SUCCESS)

    def command_land(self, command):
        exec_duration = 2.0  # finish any old commands first
        self.timeHelper.sleep(exec_duration)
        self.vehicles.land(targetHeight=0.06, duration=command.duration_in_seconds())
        self.timeHelper.sleep(command.exec_duration_in_seconds())
        command.set_result(Result.SUCCESS)

    def command_move(self, command):
        yaw = 0
        vehicle_id = command.vehicle_id
        vehicle = self.vehicles.crazyfliesById[vehicle_id]
        vehicle.goTo(command.waypoint, yaw, command.duration_in_seconds())
        self.timeHelper.sleep(command.exec_duration_in_seconds())
        command.set_result(Result.SUCCESS)

    @staticmethod
    def init_memcache_client():
        servers = ["127.0.0.1:11211"]
        memcache = Client(servers, debug=1)
        Server.delete_command_key(memcache)
        Server.delete_result_key(memcache)
        return memcache

    @staticmethod
    def delete_command_key(memcache):
        memcache.delete(Server.COMMAND_KEY)

    @staticmethod
    def delete_result_key(memcache):
        memcache.delete(Server.RESULT_KEY)


if __name__ == "__main__":
    server = Server()
    server.run()
