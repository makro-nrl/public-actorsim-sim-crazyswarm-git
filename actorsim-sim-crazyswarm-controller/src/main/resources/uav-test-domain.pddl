(define (domain UAV)
(:requirements :typing :durative-actions)
(:types uav waypointx waypointy waypointz Mode)

(:predicates (at ?u - uav ?wx - waypointx ?wy - waypointy ?wz - waypointz)
             (can_fly ?u - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz ?dx - waypointx ?dy - waypointy ?dz - waypointz)
             (can_takeoff ?u - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz )
             (can_land ?u - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz )
             (landed ?u - uav)
             (power_on ?u - uav)
             (visited ?u - uav ?vx - waypointx ?vy - waypointy ?vz - waypointz)
)

(:durative-action start
   :parameters (?uav - uav)
   :duration (= ?duration 10)
   :condition (at start ( landed ?uav ))
   :effect
                (at end (power_on ?uav))
)

(:durative-action stop
   :parameters (?uav - uav)
   :duration (= ?duration 10)
   :condition ( and
                    (at start (power_on ?uav))
                    (at start ( landed ?uav ))
              )
   :effect (
                at start (not(power_on ?uav))
  )
)

(:durative-action takeoff
:parameters (?uav - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz )
:duration (= ?duration 10)
:condition (and (over all (can_takeoff ?uav ?sx ?sy ?sz))
                (at start (power_on ?uav))
                (at start (at ?uav ?sx ?sy ?sz))
	    )
:effect (and
          (at start (not (at ?uav ?sx ?sy ?sz)))
          (at end (at ?uav ?sx ?sy z1))
          (at end (not (landed ? uav )))
		)
)

(:durative-action landing
:parameters (?uav - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz )
:duration (= ?duration 10)
:condition (and (over all (can_land ?uav ?sx ?sy ?sz))
                (at start (power_on ?uav))
                (at start (at ?uav ?sx ?sy ?sz))
	    )
:effect (and
          (at start (not (at ?uav ?sx ? sy ?sz)))
          (at end (at ?uav ?sx ?sy z0))
          (at end (landed ? uav ) )
		)
)

(:durative-action fly
:parameters (?uav - uav ?sx - waypointx ?sy - waypointy ?sz - waypointz ?dx - waypointx ?dy - waypointy ? dz - waypointz)
:duration (= ?duration 5)
:condition (and (over all (can_fly ?uav ?sx ?sy ?sz ?dx ?dy ?dz))
                (at start (power_on ?uav))
                (at start (at ?uav ?sx ?sy ?sz))
	    )
:effect (and
          (at start (not (at ?uav ?sx ?sy ?sz)))
          (at end (at ?uav ?dx ?dy ? dz))
          (at end (visited ?uav ?dx ?dy ? dz))
		)
)





)
