(define (problem uuvprob1234) (:domain UUV)
(:objects
	color high_res low_res - Mode
	uuv0 - uuv
	uuv1 - uuv
	waypoint0 waypoint1 waypoint2 waypoint3 - Waypoint
	)

(:init

	(at_inspection waypoint0)
	(at_inspection waypoint2)
	(at_inspection waypoint3)
	(at uuv0 waypoint0)
	(at uuv1 waypoint1)
	(available uuv0)
	(available uuv1)
	(equipped_for_inspection uuv0)
	(equipped_for_inspection uuv1)
	(can_traverse waypoint3 waypoint0)
	(can_traverse waypoint0 waypoint3)
	(can_traverse waypoint3 waypoint1)
	(can_traverse waypoint1 waypoint3)
	(can_traverse waypoint1 waypoint2)
	(can_traverse waypoint2 waypoint1)


)

(:goal (and
        (at uuv1 waypoint0)
        (at uuv0 waypoint1)
	    (have_inspection uuv1 waypoint2)
	)
)

(:metric minimize (total-time))
)
