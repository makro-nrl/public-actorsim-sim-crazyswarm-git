(define (domain UUV)
(:requirements :typing :durative-actions)
(:types uuv waypoint objective Mode)

(:predicates (at ?x - uuv ?y - waypoint)
             (can_traverse ?x - waypoint ?y - waypoint)
	         (equipped_for_inspection ?r - uuv)
             (have_inspection ?r - uuv ?w - waypoint)
             (available ?r - uuv)
             (have_image ?r - uuv ?o - objective ?m - mode)
	         (at_inspection ?w - waypoint)
)

	
(:durative-action navigate
:parameters (?uuv - uuv ?y - waypoint ?z - waypoint)
:duration (= ?duration 5)
:condition (and (over all (can_traverse ?y ?z))
                (at start (available ?uuv))
                (at start (at ?uuv ?y))
	    )
:effect (and
          (at start (not (at ?uuv ?y)))
          (at end (at ?uuv ?z))
		)
)

(:durative-action inspect
:parameters (?x - uuv ?p - waypoint)
:duration (= ?duration 10)
:condition (and (over all (at ?x ?p)) (at start (at ?x ?p)) (at start (at_inspection ?p)) (at start (equipped_for_inspection ?x)) 
		)
:effect (and  (at end (have_inspection ?x ?p)) (at end (not (at_inspection ?p)))
		)
)








)
