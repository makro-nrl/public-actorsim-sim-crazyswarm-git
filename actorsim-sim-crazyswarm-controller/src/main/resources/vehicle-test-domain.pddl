(define (domain vehicle)
(:requirements :typing :durative-actions)

(:types vehicle waypoint stat)

(:constants
  LANDED - stat
  HOVER - stat
  FLYING - stat
  SENSING - stat
  )

(:predicates
  ;;StateRelations
  (can_traverse ?x - waypoint ?y - waypoint)

  ;;StatePredicates
  (inspected ?w - waypoint)
  (phenomenon_at ?w - waypoint) ;;leads to formulate(inspected(?w))

  ;;multi-valued StateVariables where "value-" indicates the arg is the value of a StateVariable
  (at ?v - vehicle ?value-w - waypoint)
  (status ?v - vehicle ?value-s - stat)
)

(:durative-action navigate
:parameters (?v - vehicle
             ?start - waypoint
             ?end - waypoint)
:duration (= ?duration 1)
:condition (and
              (over all (can_traverse ?start ?end))
              (at start (at ?v ?start))
              (at start (status ?v HOVER))
           )
:effect (and
	     (at start (not (at ?v ?start)))
	     (at start (not (status ?v HOVER)))
	     (at start (status ?v FLYING))
	     (at end (not (status ?v FLYING)))
	     (at end (status ?v HOVER))
	     (at end (at ?v ?end))
	)
)

(:durative-action inspect
:parameters (?v - vehicle ?w - waypoint)
:duration (= ?duration 3)
:condition (and
             (over all (at ?v ?w))
	     (at start (status ?v HOVER))
	   )
:effect (and
	     (at start (not (status ?v HOVER)))
	     (at start (status ?v SENSING))
	     (at end (not (status ?v SENSING)))
	     (at end (status ?v HOVER))
	     (at end (inspected ?w))
	)
)

(:durative-action takeoff
:parameters (?v - vehicle)
:duration (= ?duration 2)
:condition (and
             (at start (status ?v LANDED))
	   )
:effect (and
          (at start (not (status ?v LANDED)))
          (at end (status ?v HOVER))
	)
)

(:durative-action land
:parameters (?v - vehicle)
:duration (= ?duration 2)
:condition (and
             (at start (status ?v HOVER))
	   )
:effect (and
	  (at start (not (status ?v HOVER)))
          (at end (status ?v LANDED))
	)
)

)
