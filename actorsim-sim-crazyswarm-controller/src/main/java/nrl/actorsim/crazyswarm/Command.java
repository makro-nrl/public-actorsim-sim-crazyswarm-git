package nrl.actorsim.crazyswarm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nrl.actorsim.domain.Action;
import nrl.actorsim.domain.WorldObject;

import java.io.Serializable;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class Command implements Serializable {
    /**
     * Determines how long commands sent to CrazySwarm will be executed.
     * For snappy demos with 'discrete' positioning, set to < 800.
     * For smoother demos use values > 1000.
     */
    public static int DEFAULT_DURATION_IN_MILLIS = 500;

    /**
     * Determines how long the CrazySwarm server will wait after
     * executing a command.
     * It is recommended to set this above 100.
     *
     * For snappy demos, set this < 200.
     * For extreme delay between commands, set to values > 1000.
     */
    public static int DEFAULT_SLEEP_AFTER_COMMAND_IN_MILLIS = 100;

    enum Result {
        SUCCESS,
        UNKNOWN,
        FAIL
    }

    enum ActionName {
        STOP,
        START,
        TAKEOFF,
        MOVE,
        LAND,
        UNKNOWN;

        public static ActionName convertActionToName(Action action) {
            String label = action.getLabel();
            switch(label) {
                case "stop":
                    return STOP;
                case "start":
                    return START;
                case "takeoff":
                    return TAKEOFF;
                case "move":
                    return MOVE;
                case "land":
                    return LAND;
                default:
                    return UNKNOWN;
            }
        }
    }

    public static int FLYING_HEIGHT = 1;
    private static AtomicInteger NEXT_ID = new AtomicInteger();
    private static ObjectMapper mapper = new ObjectMapper();

    public Integer id;
    public ActionName action;
    public Result result = Result.UNKNOWN;
    public int vehicle_id = 1;
    public int num_vehicles = 1;
    public float[] waypoint;
    public long duration_in_millis;
    public double sleep_after_command_in_millis;
    public boolean __Command__ = true; // a "fake" member to align with the python side

    // ====================================================
    // region<Constructors and Initializers>

    public Command() {
        init();
    }

    public Command(ActionName action) {
        init();
        this.action = action;
    }

    public Command(Action actionIn) {
        init();
        if (actionIn.getLabel().equalsIgnoreCase("navigate")) {
            action = ActionName.MOVE;
            WorldObject endWaypoint = actionIn.getBinding("end");
            WorldObject vehicle = actionIn.getBinding("v");
            int id = Integer.parseInt(vehicle.getId().replace("v", ""));
            setVehicleId(id);
            Duration duration = Duration.ofMillis(DEFAULT_DURATION_IN_MILLIS);
            to(endWaypoint, duration);
        } else {
            action = ActionName.convertActionToName(actionIn);
        }
    }

    private void init() {
        id = NEXT_ID.getAndIncrement();
        Duration afterPause = Duration.ofMillis(DEFAULT_SLEEP_AFTER_COMMAND_IN_MILLIS);
        this.sleep_after_command_in_millis = afterPause.toMillis();
    }

    public void to(WorldObject waypoint, Duration duration) {
        this.waypoint = parseWaypoint(waypoint);
        this.duration_in_millis = duration.toMillis();
    }

    public void setVehicleId(int id) {
        this.vehicle_id = id;
    }

    private float[] parseWaypoint(WorldObject waypoint) {
        String id = waypoint.getId();
        String[] tokens = id.split("_");

        float x = Float.parseFloat(tokens[1]);
        float y = Float.parseFloat(tokens[2]);
        float z = FLYING_HEIGHT;

        float[] waypointVector = new float[3];
        waypointVector[0] = x;
        waypointVector[1] = y;
        waypointVector[2] = z;
        return waypointVector;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Object Overrides>

    @Override
    public String toString() {
        String value = id + ":" + action;
        if (waypoint != null) {
            value += Arrays.toString(waypoint);
        }
        value += " result:" + result;
        return value;
    }


    // endregion
    // ====================================================

    // ====================================================
    // region<Command type checking>

    public boolean is_start() {
        return action == ActionName.START;
    }

    public boolean is_stop() {
        return action == ActionName.STOP;
    }

    public boolean is_success() {
        return result == Result.SUCCESS;
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<JSON processing>

    public static Command fromJSON(String json) {
        try {
            return mapper.readValue(json, Command.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    String toJSON() {
        try {
            String json = mapper.writeValueAsString(this);
            return json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "NOT PARSED";
    }

    // endregion
    // ====================================================


}
