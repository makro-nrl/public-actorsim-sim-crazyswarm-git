package nrl.actorsim.crazyswarm;

import nrl.actorsim.goalrefinement.lifecycle.StrategyName;
import nrl.actorsim.goalrefinement.lifecycle.StrategyGroup;
import nrl.actorsim.planner.PDDLPlannerWorker;
import nrl.actorsim.strategies.ExecuteStatementStrategies;
import nrl.actorsim.strategies.PlanRequestStrategies;

public class Strategies extends StrategyGroup {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Strategies.class);

    final PDDLPlannerWorker planner;

    public Strategies(PDDLPlannerWorker planner) {
        super();
        this.planner = planner;
    }

    public void createAllInspectedStrategies() {
        addAll(new PlanRequestStrategies(Goal.createInspectedGoal(), planner));
    }

    public void createInspectedStrategies(StrategyName... strategyNames) {
        addAll(new PlanRequestStrategies(Goal.createInspectedGoal(), planner, strategyNames));
    }

    public void addAllExecuteStatementStrategies() {
        addAll(new ExecuteStatementStrategies());
    }
}
