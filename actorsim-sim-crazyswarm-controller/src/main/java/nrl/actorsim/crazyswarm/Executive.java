package nrl.actorsim.crazyswarm;

import nrl.actorsim.cognitive.ExecutiveBase;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.planner.PDDLPlannerWorker;
import nrl.actorsim.planner.PDDLPlanRequestWithMemory;
import nrl.actorsim.planner.POPFPlannerWorker;

import java.util.Collection;

import static nrl.actorsim.crazyswarm.PlanningDomain.VEHICLE;

public class Executive extends ExecutiveBase<PlanningDomain> {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Executive.class);

    private static Executive instance;

    public static void reset() {
        instance = null;
    }

    @SuppressWarnings("unused")
    public static Executive getInstance() {
        if (instance == null) {
            PlanningDomain.Options domainOptions = PlanningDomain.Options.builder()
                .name("hubo")
                .closedWorldAssumption()
                .build();
            getInstance(new PlanningDomain(domainOptions));
        }
        return instance;
    }

    public static Executive getInstance(PlanningDomain domain) {
        instance = new Executive(domain);
        instance.start();
        return instance;
    }

    private Executive(PlanningDomain domain) {
        super("CSExec", domain);
        POPFPlannerWorker planner = POPFPlannerWorker.findPopfPlannerWorker(domain);
        setPlanner(planner);
    }

    public PDDLPlanRequestWithMemory getRequestWithMemory() {
        return new PDDLPlanRequestWithMemory(memory, domain);
    }

    public int getNumVehicles() {
        Collection<WorldObject> vehicles = memory.find(
                worldObject -> worldObject.typeEquals(VEHICLE));
        return vehicles.size();
    }

    @Override
    public PDDLPlannerWorker getPlanner() {
        return (PDDLPlannerWorker) super.getPlanner();
    }
}
