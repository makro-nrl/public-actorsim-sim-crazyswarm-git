package nrl.actorsim.crazyswarm;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.MemcachedNode;
import nrl.actorsim.domain.Action;
import nrl.actorsim.domain.ExecuteStatement;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goals.ExecuteStatementGoal;
import nrl.actorsim.goalrefinement.lifecycle.GoalMode;
import nrl.actorsim.utils.WorkerThread;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.*;

import static nrl.actorsim.domain.ExecuteStatement.State.COMPLETED;
import static nrl.actorsim.domain.ExecuteStatement.State.EXECUTING;

public class CrazySwarmClient extends WorkerThread {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(CrazySwarmClient.class);

    public static String COMMAND_KEY = "crazyswarm_command";
    public static String RESULT_KEY = "crazyswarm_result";
    public static Duration WAKE_UP_TIMEOUT = Duration.ofMillis(250);

    final Executive executive;
    final MemcachedClient memcache;
    boolean waitingOnResult = false;
    boolean missionStarted = false;

    private Map<Integer, ExecuteStatementGoal> commandIdNodeMap = new TreeMap<>();

    CrazySwarmClient(Executive executive) throws IOException, InterruptedException {
        super(WorkerThread.Options.builder().shortName("CSClient").build());

        this.executive = executive;
        memcache = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
        Thread.sleep(1000); //wait for the client to connect
        checkMemcacheServer();
        memcache.delete(COMMAND_KEY);
        memcache.delete(RESULT_KEY);
        start();
        setWakeUpCall(CrazySwarmClient.WAKE_UP_TIMEOUT);
    }

    private void checkMemcacheServer() {
        boolean activeNodeFound = false;
        for (MemcachedNode node : memcache.getNodeLocator().getAll()) {
            if (node.isActive()) {
                activeNodeFound = true;
            }
        }
        if (! activeNodeFound) {
            String msg = "Failed to connect to Memcached server.  Did you forget to start memcached?";
            logger.error(msg);
            memcache.shutdown();
            throw new UnsupportedOperationException(msg);
        }
    }

    @Override
    public WorkResult performWork() {
        logger.debug("Checking for new work...");
        if (waitingOnResult) {
            Command result = getResult();
            if (result != null) {
                processResult(result);
                waitingOnResult = false;
            }
        } else if (!missionStarted) {
            Command startCommand = getStartCommand(executive.getNumVehicles());
            sendCommand(startCommand);
            waitingOnResult = true;
        } else {
            Command nextCommand = getNextCommand();
            if (nextCommand != null) {
                sendCommand(nextCommand);
                waitingOnResult = true;
            }
        }
        setWakeUpCall(WAKE_UP_TIMEOUT);
        return WorkResult.CONTINUE;
    }

    public Command getResult() {
        checkMemcacheServer();
        Object resultRaw = memcache.get(RESULT_KEY);
        if (resultRaw != null) {
            memcache.delete(RESULT_KEY);
            return Command.fromJSON(resultRaw.toString());
        }
        return null;
    }

    private void processResult(Command result) {
        logger.debug("Processing result '{}'", result);
        if (result.is_start()) {
            missionStarted = true;
        } else if (result.is_stop()) {
            missionStarted = false;
        } else if (result.is_success()) {
            if (commandIdNodeMap.containsKey(result.id)) {
                ExecuteStatementGoal execGoal = commandIdNodeMap.get(result.id);
                ExecuteStatement statement = execGoal.content();
                Duration timeout = Duration.ofSeconds(10);
                statement.attemptTransitionToAndNotify(COMPLETED, timeout);
                executive.notifyWorker();
            }
        }
    }

    private void sendCommand(Command command) {
        logger.debug("Sending command '{}'", command);
        String commandJson = command.toJSON();
        checkMemcacheServer();
        memcache.set(COMMAND_KEY, 0, commandJson);
    }

    private Command getStartCommand(int numVehicles) {
        Command command = new Command(Command.ActionName.START);
        command.num_vehicles = numVehicles;
        return command;
    }

    private Command getNextCommand() {
        Set<GoalLifecycleNode> readyForExecution = executive.getGoalMemory().find(
                node -> (node instanceof ExecuteStatementGoal)
                        && node.modeMatches(GoalMode.DISPATCHED)
                        && node.isUnconstrained());
        for (GoalLifecycleNode node : readyForExecution) {
            logger.debug("Processing next command for {}", node);
            if (node instanceof ExecuteStatementGoal
                    && node.isNotComplete()) {

                ExecuteStatementGoal execGoal = (ExecuteStatementGoal) node;
                ExecuteStatement statement = node.content();

                Action.AsWorldObject actionHolder = statement.getBinding("action").content();
                Action action = actionHolder.getItem();

                if (action.getLabel().equalsIgnoreCase("inspect")) {
                    logger.debug("Marking inspect complete because crazyswarm does not have an inspect command.");
                    statement.attemptTransitionToAndNotify(COMPLETED);
                    break;
                }

                Command command = new Command(action);
                commandIdNodeMap.put(command.id, execGoal);

                Duration timeout = Duration.ofSeconds(10);
                statement.attemptTransitionToAndNotify(EXECUTING, timeout);
                return command;
            }
        }
        return null;
    }
}
