package nrl.actorsim.crazyswarm;

import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.test.StrategyThreadHelper;
import nrl.actorsim.goals.RequestGoal;
import nrl.actorsim.memory.WorkingMemory;

import java.util.Collection;

import static nrl.actorsim.crazyswarm.PlanningDomain.*;
import static nrl.actorsim.goalrefinement.goals.StateVariableGoal.CompleteWhen.*;

public class Goal extends RequestGoal {
    boolean planUsingRequest;

    // ====================================================
    // region<Constructors and Init>

    @SuppressWarnings("unused")
    Goal(Statement goalStatement, CompleteWhen completionWhen) {
        super(goalStatement, completionWhen);
        initStartTime();
        planUsingRequest = false;
    }

    Goal(Statement goalStatement,
         CompleteWhen completionWhen,
         Statement formulateCondition) {
        super(goalStatement, completionWhen, formulateCondition);
        initStartTime();
        planUsingRequest = false;
    }


    protected Goal(Goal template, Bindings bindings) {
        super(template.stored.cloner().build(), template.completeWhen);
        stored.bindArgs(bindings);
        initStartTime();
        planUsingRequest = template.planUsingRequest;
    }

    protected void initStartTime() {
        stored.estimatedStartEqualsEnd();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Overridden methods>

    @Override
    public Goal instance(Bindings bindings) {
        return new Goal(this, bindings);
    }

    @Override
    public boolean needsRequest() {
        return super.needsRequest()
                && planUsingRequest;
    }

    // endregion
    // ====================================================


    // ====================================================
    // region<Static creators>

    public static Goal createInspectedGoal(WorldObject worldObject) {
        Goal g = createAchievementGoal(INSPECTED.persistenceStatement(worldObject),
                PHENOMENON_AT.persistenceStatement(worldObject));
        g.planUsingRequest = true;
        return g;
    }

    public static Goal createInspectedGoal() {
        Goal g = createAchievementGoal(INSPECTED.persistenceStatement(),
                PHENOMENON_AT.persistenceStatement());
        g.planUsingRequest = true;
        return g;
    }


    public static void addReturnToBaseGoals(Executive executive) throws InterruptedException {
        WorkingMemory memory = executive.getWorkingMemory();
        Collection<WorldObject> vehicles = memory.getObjects(
                worldObject -> worldObject.equalsOrInheritsFromType(VEHICLE)
        );

        WorldObject landedStat = memory.getObject("landed");
        for (WorldObject vehicle : vehicles) {
            //noinspection unchecked
            Goal landed = Goal.createAchievementGoal(
                    STATUS.persistenceStatement(vehicle).assign(landedStat));
            StrategyThreadHelper.formulate(landed, memory.getGoalMemory());
            StrategyThreadHelper.select(landed);

            WorldObject base = memory.getObject("waypoint_0_0");
            //noinspection unchecked
            Goal atBase = Goal.createAchievementGoal(
                    AT.persistenceStatement(vehicle).assign(base));
            StrategyThreadHelper.formulate(atBase, memory.getGoalMemory());
            StrategyThreadHelper.select(atBase);
        }
    }

    public static Goal createAchievementGoal(Statement goalStatement,
                                             Statement formulateCondition) {
        return new Goal(goalStatement, PRESENT_IN_MEMORY, formulateCondition);
    }

    public static Goal createAchievementGoal(Statement goalStatement) {
        return new Goal(goalStatement, PRESENT_IN_MEMORY);
    }

    // endregion
    // ====================================================

}
