package nrl.actorsim.crazyswarm;

import nrl.actorsim.domain.*;

public class PlanningDomain extends PDDLPlanningDomain {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PlanningDomain.class);

    // ====================================================
    // region<Common>

    //WorldTypes
    public static WorldType WAYPOINT;
    public static WorldType VEHICLE;

    //static constant world objects
    public static WorldObject LANDED;
    public static WorldObject FLYING;

    //StateRelations
    public static StateVariableTemplate CAN_TRAVERSE;

    //StatePredicates
    public static StateVariableTemplate INSPECTED;
    public static StateVariableTemplate PHENOMENON_AT;

    //multi-valued StateVariables
    public static StateVariableTemplate AT;
    public static StateVariableTemplate STATUS;
    // endregion
    // ====================================================

    // ====================================================
    // region<UAV>
    public static WorldType UAV;

//TODO unify examples with Vivint's PDDL
    public static WorldType MODE;
    public static StateVariableTemplate CAN_FLY;
    public static StateVariableTemplate CAN_TAKEOFF;
    public static StateVariableTemplate CAN_LAND;
    public static StateVariableTemplate POWER_ON;
    // endregion
    // ====================================================

    // ====================================================
    // region<UUV>
    public static WorldType UUV;
    public static WorldType CAMERA;
    public static WorldType SHIP;
    public static WorldType OBJECTIVE;

    public static StateVariableTemplate AT_SHIP;
    public static StateVariableTemplate AVAILABLE;
    public static StateVariableTemplate VISIBLE;

    // endregion
    // ====================================================

    public PlanningDomain(Options options) {
        super(options);
    }


    @Override
    public void loadPDDL() {
        super.loadPDDL();
        loadCommon();
        if (getName().equalsIgnoreCase("uav")) {
            loadUAV();
        }
        if (getName().equalsIgnoreCase("uuv")) {
            loadUUV();
        }

        logger.debug("Loaded domain {}", this);
    }

    private void loadCommon() {
        WAYPOINT = TypeManager.get("WAYPOINT");
        VEHICLE = TypeManager.get("VEHICLE");

        LANDED = getConstantObject("LANDED");
        FLYING = getConstantObject("FLYING");

        //StateRelations
        CAN_TRAVERSE = getPredicate("can_traverse");

        INSPECTED = getPredicate("inspected");

        AT = getPredicate("at");
        STATUS = getPredicate("status");
        PHENOMENON_AT = getPredicate("phenomenon_at");

    }

    private void loadUAV() {
        UAV = TypeManager.get("UAV");

        MODE = TypeManager.get("MODE");
        CAN_FLY = getPredicate("can_fly");
        CAN_TAKEOFF = getPredicate("can_takeoff");
        CAN_LAND = getPredicate("can_land");
//        LANDED = getPredicate("landed");
        POWER_ON = getPredicate("power_on");
    }

    private void loadUUV() {
        UUV = TypeManager.get("UUV");
        MODE = TypeManager.get("MODE");
        CAMERA = TypeManager.get("CAMERA");
        SHIP = TypeManager.get("SHIP");
        OBJECTIVE = TypeManager.get("OBJECTIVE");

        AT_SHIP = getPredicate("at_ship");
        AVAILABLE = getPredicate("available");
        VISIBLE = getPredicate("visible");
    }
}
