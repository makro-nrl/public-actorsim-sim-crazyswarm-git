package nrl.actorsim.crazyswarm;

import nrl.actorsim.domain.*;
import nrl.actorsim.memory.*;
import nrl.actorsim.planner.PDDLPlannerWorker;

import java.nio.file.*;
import java.util.*;

import static nrl.actorsim.crazyswarm.PlanningDomain.*;

public class TestExecutiveHelper {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestExecutiveHelper.class);

    Options options;

    PlanningDomain domain;
    PlanningProblem problem;
    Executive executive;
    WorkingMemory memory;
    GoalMemorySimple goalMemory;
    InitialObjects init;
    Strategies strategies;

    TestExecutiveHelper(String testPrefix) {
        this(new Options(testPrefix, "emptyProblem"));
    }

    TestExecutiveHelper(String testPrefix, String problemName) {
        this(new Options(testPrefix, problemName));
    }

    TestExecutiveHelper(Options options) {
        this.options = options;
        domain = loadDomain(options.testPrefix, options.problemName);
        problem = domain.getProblem();
        executive = Executive.getInstance(domain);
        memory = executive.getWorkingMemory();
        goalMemory = executive.getGoalMemory();
        strategies = new Strategies(executive.getPlanner());
    }

    // ====================================================
    // region<Object Related>

    void initObjectsAndNotify() {
        initObjects();
        executive.notifyWorkerAndWaitOnCycle();
        PDDLPlannerWorker planner = executive.getPlanner();
        if (planner != null) {
            planner.waitUntilQueueEmpty();
        }
    }

    void initObjects() {
        this.init = new InitialObjects(this);
        init.insertInitialState();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Domain loading>

    public static PlanningDomain loadDomain(String prefix) {
        PlanningDomain.Options options = PlanningDomain.Options.builder()
                .closedWorldAssumption()
                .domainFilename(getTestDomainPath(prefix))
                .problemFilename(getTestProblemPath(prefix))
                .build();
        PlanningDomain domain = new PlanningDomain(options);
        domain.loadPDDL();
        logger.debug("Loaded domain {}", domain);
        return domain;
    }

    public static PlanningDomain loadDomain(String prefix, String problem) {
        PlanningDomain.Options options = PlanningDomain.Options.builder()
                .closedWorldAssumption()
                .domainFilename(getTestDomainPath(prefix))
                .problemFilename(getTestProblemPath(prefix, problem))
                .build();
        PlanningDomain domain = new PlanningDomain(options);
        domain.loadPDDL();
        logger.debug("Loaded domain {}", domain);
        return domain;
    }

    public static Path getTestDomainPath(String prefix) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-domain.pddl");
        logger.info("Parsing domain {}", path);
        return path;
    }

    public static Path getTestProblemPath(String prefix) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-navigate-problem.pddl");
        logger.info("Parsing problem {}", path);
        return path;
    }

    public static Path getTestProblemPath(String prefix, String problem) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-" + problem + ".pddl");
        logger.info("Parsing problem {}", path);
        return path;
    }

    private static Path getBasePath() {
        String cwd = System.getProperty("user.dir");
        Path cwdAsPath = Paths.get(cwd);
        return cwdAsPath.resolve("src/main/resources/");
    }

    // endregion
    // ====================================================

    static class Options {
        String testPrefix;
        String problemName = "emptyProblem";

        int GRID_SIZE = 1;
        int numVehicles = 1;

        String[] vehicleStarts = new String[] { "waypoint_0_0", "waypoint_0_0", "waypoint_0_0" };
        String[] phenomena = new String[] { "waypoint_1_1" };

        @SuppressWarnings("unused")
        public Options() {

        }

        public Options(String testPrefix) {
            this.testPrefix = testPrefix;
        }

        public Options(String testPrefix, String problemName) {
            this.testPrefix = testPrefix;
            this.problemName = problemName;
        }

    }

    static class InitialObjects extends InitObjectsHelper<PlanningDomain, Executive> {
        TestExecutiveHelper parent;
        List<Statement> initStatements;

        InitialObjects(TestExecutiveHelper parent) {
            super(parent.domain, parent.executive);
            this.parent = parent;
        }

        @Override
        public List<Statement> createInitStatements() {
            initStatements = new ArrayList<>();
            generateGrid();
            addVehicles();
            addPhenomena();

            for (Statement s : initStatements) {
                //initial time of the world is assumed to be at [-INF, -INF]
                s.estimatedEndEqualsStart();
            }
            return initStatements;
        }

        void generateGrid() {
            int GRID_SIZE = parent.options.GRID_SIZE;

            //add waypoints
            for (int x = 0; x <= GRID_SIZE; x++) {
                for (int y = 0; y <= GRID_SIZE; y++) {
                    String name = "waypoint_" + x + "_" + y;
                    problem.add(WAYPOINT.instance(name));
                }
            }

            //add traverse for inner grid
            for (int x = 0; x < GRID_SIZE; x++) {
                for (int y = 0; y < GRID_SIZE; y++) {
                    String start = "waypoint_" + x + "_" + y;
                    addTraverse(start, up(x, y));
                    addTraverse(start, right(x, y));
                }
            }

            //add the final corner
            String end = "waypoint_" + GRID_SIZE + "_" + GRID_SIZE;
            addTraverse(end, left(GRID_SIZE, GRID_SIZE));
            addTraverse(end, down(GRID_SIZE, GRID_SIZE));
        }

        private String up(int x, int y) {
            return "waypoint_" + x + "_" + (y+1);
        }

        private String down(int x, int y) {
            return "waypoint_" + x + "_" + (y-1);
        }

        private String left(int x, int y) {
            return "waypoint_" + (x-1) + "_" + y;
        }

        private String right(int x, int y) {
            return "waypoint_" + (x+1) + "_" + y;
        }

        void addTraverse(String src, String dest) {
            WorldObject start = problem.getObject(src);
            WorldObject end = problem.getObject(dest);

            initStatements.add(CAN_TRAVERSE.persistenceStatement(start, end));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(end, start));
        }

        void addVehicles() {
            WorldObject landed = problem.getObject("landed");

            int numVehicles = parent.options.numVehicles;
            String[] starts = parent.options.vehicleStarts;
            for (int id = 1; id <= numVehicles; ++id) {
                String vehicleName = "v" + id;
                WorldObject vehicle = VEHICLE.instance(vehicleName);
                problem.add(vehicle);
                if (vehicle != null) {
                    WorldObject start = problem.getObject(starts[id-1]);
                    //noinspection unchecked
                    initStatements.add(AT.persistenceStatement(vehicle).assign(start));
                    //noinspection unchecked
                    initStatements.add(STATUS.persistenceStatement(vehicle).assign(landed));
                }
            }
        }

        void addPhenomena() {
            for (String phenomenon : parent.options.phenomena) {
                WorldObject waypoint = problem.getObject(phenomenon);
                initStatements.add(PHENOMENON_AT.persistenceStatement(waypoint));
            }
        }
    }
}
