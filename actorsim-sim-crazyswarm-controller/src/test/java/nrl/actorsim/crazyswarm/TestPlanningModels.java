package nrl.actorsim.crazyswarm;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import nrl.actorsim.planner.POPFPlannerWorker;
import nrl.actorsim.planner.PlanRequest;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class TestPlanningModels {

    public static void enableDebugLogging() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger("nrl.actorsim");
        rootLogger.setLevel(Level.DEBUG);
    }

    @Test
    public void testValidPlanForSatelliteSimpleTimeProblem() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/ipc02-satelliteSimpleTime-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/ipc02-satelliteSimpleTime-problem01.pddl");
        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);

        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 10;
    }

    @Test
    public void testValidPlanForSatelliteWithTimeWindowAsTILs() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/ipc04-satelliteWindowsAsTILs-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/ipc04-satelliteWindowsAsTILs-problem01.pddl");

        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);
        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 13;
    }

    @Test
    public void testValidPlanForUUVDomain() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/uuv-test-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/uuv-test-navigate-problem.pddl");

        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);
        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 9;
    }

    @Test
    public void testValidPlanForUUVProblem2() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/uuv-test-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/uuv-test-navigate-problem2.pddl");

        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);
        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 7;
    }

    @Test
    public void testValidPlanForSimpleVehicle() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/vehicle-test-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/vehicle-test-problem1.pddl");

        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);
        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 8;
    }

    @Test
    public void testValidPlanForAUVDomain() throws IOException {
        enableDebugLogging();

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path domainPath = cwd.resolve("src/main/resources/uav-test-domain.pddl");
        Path problemPath = cwd.resolve("src/main/resources/uav-test-navigate-problem.pddl");

        POPFPlannerWorker worker = POPFPlannerWorker.findAndStartPopfPlannerWorkerWithNullDomain();
        PlanRequest planRequest = createRequest(domainPath, problemPath);
        worker.enqueue(planRequest);
        worker.notifyWorkerAndWaitOnCycle();
        worker.waitUntilQueueEmpty();
        List<String> plan = planRequest.getPlan();
        assert plan.size() == 8;
    }

    @NotNull
    private PlanRequest createRequest(Path domainPath, Path problemPath) throws IOException {
        PlanRequest planRequest = new PlanRequest();
        planRequest.readDomain(domainPath);
        planRequest.readProblem(problemPath);
        planRequest.enableCapturePlannerOutput();
        return planRequest;
    }


}
