package nrl.actorsim.crazyswarm;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goals.ExecuteStatementGoal;
import nrl.actorsim.planner.PDDLPlanRequestWithMemory;
import nrl.actorsim.planner.PlannerWorkerBase;
import nrl.actorsim.planner.PlanRequestWithMemory;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;

import static nrl.actorsim.crazyswarm.PlanningDomain.*;
import static nrl.actorsim.domain.ExecuteStatement.State.COMPLETED;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;

public class TestExecutive3 {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(TestExecutive.class);

    public static void enableDebugLogging() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger("nrl.actorsim");
        rootLogger.setLevel(Level.DEBUG);
    }

    @BeforeMethod
    public void reset() {
        enableDebugLogging();
        Executive.reset();
    }

    @Test
    public void testLoadDomain() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        Collection<Operator> operators = t.domain.getOperators();
        Assert.assertEquals(operators.size(),4);
        Operator op = t.domain.getOperator("navigate");

        Collection<Statement> conditions = op.getPreconditions();
        assert conditions.size() == 3;


        Collection<Statement> effects = op.getEffects();
        assert effects.size() == 2;
    }

    @Test
    public void testActionInstance() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        for (Operator operator : t.domain.getOperators()) {
            Action action = operator.instance();
            assert action.getPreconditions().size() == operator.getPreconditions().size();
            assert action.getEffects().size() == operator.getEffects().size();
        }
    }

    @Test
    public void testConfirmWaypointInMemory() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");

        WorldObject w_3_1 = t.problem.getObject("waypoint_3_1");
        t.memory.assertMissing(w_3_1);

        double simulationTime = 1;
        t.memory.updateOrCloneThenAdd(w_3_1, simulationTime);
        t.memory.assertContains(w_3_1);
    }

    @Test
    public void testInsertPhenomenon() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");

        WorldObject w_3_1 = t.problem.getObject("waypoint_3_1");
        t.memory.assertMissing(w_3_1);

        double simulationTime = 1;
        t.memory.updateOrCloneThenAdd(w_3_1, simulationTime);
        t.memory.assertContains(w_3_1);
    }

    @Test
    public void testInsertInitialState() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        t.initObjectsAndNotify();

        Duration timeout = Duration.ofSeconds(5);
        t.executive.notifyWorkerAndWaitOnCycle(timeout);

        for (Statement statement : t.init.createInitStatements()) {
            t.memory.assertContains(statement);
        }
    }

    @Test
    public void testBasicFormulateStrategy() {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        t.strategies.createInspectedStrategies(FORMULATE);
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        PlanningProblem problem = t.domain.getProblem();
        WorldObject w_3_1 = problem.getObject("waypoint_3_1");
        Statement phenomenonAt = PHENOMENON_AT.persistenceStatement(w_3_1);
        WorldObject w_2_4 = problem.getObject("waypoint_2_4");
        Statement phenomenonAt_2_4 = PHENOMENON_AT.persistenceStatement(w_2_4);
        t.executive.assertContainsAfterStateUpdate(phenomenonAt);
        t.executive.assertContainsAfterStateUpdate(phenomenonAt_2_4);
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal tmpGoal1 = Goal.createInspectedGoal(w_3_1);
        t.goalMemory.assertContains(tmpGoal1);
        Goal tmpGoal2 = Goal.createInspectedGoal(w_2_4);
        t.goalMemory.assertContains(tmpGoal2);
        assert t.goalMemory.getNumberOfGoals() == 3;
        t.goalMemory.assertModeEquals(tmpGoal1, FORMULATED);
    }

    @Test
    public void testInitialPlanIsValid() throws IOException, InterruptedException {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        t.initObjectsAndNotify();

        t.init.insertAndSelect(Goal.createInspectedGoal(t.init.w_3_1));
        //noinspection unchecked
        Goal landed = Goal.createAchievementGoal(STATUS.persistenceStatement(t.init.v0).assign(t.init.landed));
        t.init.insertAndSelect(landed);
        //noinspection unchecked
        Goal atBase = Goal.createAchievementGoal(AT.persistenceStatement(t.init.v0).assign(t.init.w_0_0));
        t.init.insertAndSelect(atBase);
        t.executive.notifyWorkerAndWaitOnCycle(); //update memory with state

        t.init.insertAndSelect(Goal.createInspectedGoal(t.init.w_2_4));
        //noinspection unchecked
        Goal landedv1 = Goal.createAchievementGoal(STATUS.persistenceStatement(t.init.v1).assign(t.init.landed));
        t.init.insertAndSelect(landedv1);
        //noinspection unchecked
        Goal atBasev1 = Goal.createAchievementGoal(AT.persistenceStatement(t.init.v1).assign(t.init.w_0_0));
        t.init.insertAndSelect(atBasev1);

        t.executive.notifyWorkerAndWaitOnCycle(); //update memory with state


        Goal tmpGoal = Goal.createInspectedGoal(t.init.w_3_1);
        t.goalMemory.assertModeEquals(tmpGoal, SELECTED);
        Goal tmpGoal2 = Goal.createInspectedGoal(t.init.w_2_4);
        t.goalMemory.assertContains(tmpGoal2);
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        PlanRequestWithMemory request = new PDDLPlanRequestWithMemory(t.memory, t.domain);
        request.readDomain(t.domain.getPddlOptions().domainPath);
        request.generateProblem();
        request.enableCapturePlannerOutput();
        PlannerWorkerBase planner = t.executive.getPlanner();
        planner.enqueue(request);
        planner.notifyWorkerAndWaitOnCycle();
        planner.waitUntilQueueEmpty();
        List<String> plan = request.getPlan();
        assert plan.size() == 27;
    }

    @Test
    public void testCommitCorrectlyCreatesExecutionStatements() throws InterruptedException {
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createInspectedStrategies(FORMULATE, SELECT, EXPAND, COMMIT);
        t.strategies.addStrategies(t.memory);
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal tmpGoal = Goal.createInspectedGoal(t.init.w_3_1);
        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == COMMITTED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == COMMITTED;
        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 26;
        assert t.goalMemory.getNumberOfGoals() == 32;
    }

    @Test
    public void testExecutionSucceedsWithoutExecutive() throws InterruptedException {
/* =================================================================
   =================================================================
   TODO fix this test failing if run as part of the entire class but succeeding when run alone
        Most likely this is just an initialization problem with the setup()
   =================================================================
   =================================================================
 */
        TestExecutiveHelper3 t = new TestExecutiveHelper3("vehicle-test", "problem3");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createAllInspectedStrategies();
        t.strategies.addAllExecuteStatementStrategies();
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        Goal tmpGoal = Goal.createInspectedGoal(t.init.w_3_1);

        logger.debug(t.goalMemory.toString());
        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == DISPATCHED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == DISPATCHED;

        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 26;
        assert t.goalMemory.getNumberOfGoals() == 32;

        Duration timeout = Duration.ofSeconds(10);
        Instant start = Instant.now();

        while (children.length > 0) {
            logger.debug(t.goalMemory.toString());
            for (GoalLifecycleNode node : children) {
                if (node.modeMatches(DISPATCHED)
                        && node.isNotComplete() ) {
                    if (node instanceof ExecuteStatementGoal) {
                        //complete the first one found that is dispatched and incomplete
                        ExecuteStatement statement = node.content();
                        statement.attemptTransitionToAndNotify(COMPLETED);
                        Action action = statement.getAction();
                        logger.debug("Transition to COMPLETED action '{}' with step {}", action.getLabel(), statement.getPlanStep());
                    }

                    break;
                }
            }
            Thread.sleep(1000);
            children = goalInMemory.getSubgoals();
            Duration elapsed = Duration.between(Instant.now(), start);
            assert elapsed.compareTo(timeout) < 0;
        }
        children = goalInMemory.getSubgoals();
        assert children.length == 0;
    }

    @Test
    public void testExecutionSucceedsWithExecutive() throws InterruptedException, IOException {
        TestExecutiveHelper2 t = new TestExecutiveHelper2("vehicle-test", "problem2");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createAllInspectedStrategies();
        t.strategies.addAllExecuteStatementStrategies();
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        Goal tmpGoal = Goal.createInspectedGoal(t.init.w_2_2);

        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == DISPATCHED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == DISPATCHED;

        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 11;

        Duration timeout = Duration.ofSeconds(children.length * 3);
        Instant start = Instant.now();

        //noinspection unused
        CrazySwarmClient client = new CrazySwarmClient(t.executive);

        while (children.length > 0) {
            logger.info("Waiting for CrazySwarmClient to send commands");
            Thread.sleep(5000);
            children = goalInMemory.getSubgoals();
            Duration elapsed = Duration.between(Instant.now(), start);
            assert elapsed.compareTo(timeout) < 0;
        }
        children = goalInMemory.getSubgoals();
        assert children.length == 0;
    }

}
