package nrl.actorsim.crazyswarm;

import nrl.actorsim.domain.*;
import nrl.actorsim.memory.GoalMemorySimple;
import nrl.actorsim.memory.WorkingMemory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static nrl.actorsim.crazyswarm.PlanningDomain.*;

public class TestExecutiveHelper3 {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestExecutiveHelper.class);

    PlanningDomain domain;
    PlanningProblem problem;
    Executive executive;
    WorkingMemory memory;
    GoalMemorySimple goalMemory;
    InitialObjects3 init;
    Strategies strategies;

    TestExecutiveHelper3(String testPrefix, String problemName) {
        domain = loadDomain(testPrefix, problemName);
        problem = domain.getProblem();
        executive = Executive.getInstance(domain);
        memory = executive.getWorkingMemory();
        goalMemory = executive.getGoalMemory();
        strategies = new Strategies(executive.getPlanner());
    }

    // ====================================================
    // region<Object Releated>

    void initObjectsAndNotify() {
        initObjects();
        executive.notifyWorkerAndWaitOnCycle();
        executive.getPlanner().waitUntilQueueEmpty();
    }

    void initObjects() {
        this.init = new InitialObjects3(domain, executive);
        init.insertInitialState();
    }

    // endregion
    // ====================================================

    // ====================================================
    // region<Domain loading>

    public static PlanningDomain loadDomain(String prefix) {
        PlanningDomain.Options options = PlanningDomain.Options.builder()
                .closedWorldAssumption()
                .domainFilename(getTestDomainPath(prefix))
                .problemFilename(getTestProblemPath(prefix))
                .build();
        PlanningDomain domain = new PlanningDomain(options);
        domain.loadPDDL();
        logger.debug("Loaded domain {}", domain);
        return domain;
    }

    public static PlanningDomain loadDomain(String prefix, String problem) {
        PlanningDomain.Options options = PlanningDomain.Options.builder()
                .closedWorldAssumption()
                .domainFilename(getTestDomainPath(prefix))
                .problemFilename(getTestProblemPath(prefix, problem))
                .build();
        PlanningDomain domain = new PlanningDomain(options);
        domain.loadPDDL();
        logger.debug("Loaded domain {}", domain);
        return domain;
    }

    public static Path getTestDomainPath(String prefix) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-domain.pddl");
        logger.info("Parsing domain {}", path);
        return path;
    }

    public static Path getTestProblemPath(String prefix) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-navigate-problem.pddl");
        logger.info("Parsing problem {}", path);
        return path;
    }

    public static Path getTestProblemPath(String prefix, String problem) {
        Path basePath = getBasePath();
        Path path = basePath.resolve(prefix + "-" + problem + ".pddl");
        logger.info("Parsing problem {}", path);
        return path;
    }

    private static Path getBasePath() {
        String cwd = System.getProperty("user.dir");
        Path cwdAsPath = Paths.get(cwd);
        return cwdAsPath.resolve("src/main/resources/");
    }



    /**
     * Sets up an initial world with the following predicates:
     *   (at v0 waypoint_0_0)
     *   (can_traverse waypoint_0_0 waypoint_0_1)
     *   (can_traverse waypoint_0_1 waypoint_0_0)
     *
     *   (can_traverse waypoint_0_0 waypoint_1_0)
     *   (can_traverse waypoint_1_0 waypoint_0_0)
     *
     *   (can_traverse waypoint_1_0 waypoint_1_1)
     *   (can_traverse waypoint_1_1 waypoint_1_0)
     *
     *   (can_traverse waypoint_0_1 waypoint_1_1)
     *   (can_traverse waypoint_1_1 waypoint_0_1)
     *
     *	 (phenomena_at waypoint_1_1)
     */
    static class InitialObjects3 extends InitObjectsHelper<PlanningDomain, Executive> {
        WorldObject v0;
        WorldObject v1;

        WorldObject flying;
        WorldObject landed;

        WorldObject w_0_0;
        WorldObject w_0_1;
        WorldObject w_0_2;
        WorldObject w_0_3;
        WorldObject w_0_4;
        WorldObject w_1_0;
        WorldObject w_1_1;
        WorldObject w_1_2;
        WorldObject w_1_3;
        WorldObject w_1_4;
        WorldObject w_2_0;
        WorldObject w_2_1;
        WorldObject w_2_2;
        WorldObject w_2_3;
        WorldObject w_2_4;
        WorldObject w_3_0;
        WorldObject w_3_1;
        WorldObject w_3_2;
        WorldObject w_3_3;
        WorldObject w_3_4;
        WorldObject w_4_0;
        WorldObject w_4_1;
        WorldObject w_4_2;
        WorldObject w_4_3;
        WorldObject w_4_4;

        InitialObjects3(PlanningDomain domain, Executive executive) {
            super(domain, executive);
            v0 = problem.getObject("v0");
            v1 = problem.getObject("v1");
            flying = problem.getObject("flying");
            landed = problem.getObject("landed");
            w_0_0 = problem.getObject("waypoint_0_0");
            w_0_1 = problem.getObject("waypoint_0_1");
            w_0_2 = problem.getObject("waypoint_0_2");
            w_0_3 = problem.getObject("waypoint_0_3");
            w_0_4 = problem.getObject("waypoint_0_4");
            w_1_0 = problem.getObject("waypoint_1_0");
            w_1_1 = problem.getObject("waypoint_1_1");
            w_1_2 = problem.getObject("waypoint_1_2");
            w_1_3 = problem.getObject("waypoint_1_3");
            w_1_4 = problem.getObject("waypoint_1_4");
            w_2_0 = problem.getObject("waypoint_2_0");
            w_2_1 = problem.getObject("waypoint_2_1");
            w_2_2 = problem.getObject("waypoint_2_2");
            w_2_3 = problem.getObject("waypoint_2_3");
            w_2_4 = problem.getObject("waypoint_2_4");
            w_3_0 = problem.getObject("waypoint_3_0");
            w_3_1 = problem.getObject("waypoint_3_1");
            w_3_2 = problem.getObject("waypoint_3_2");
            w_3_3 = problem.getObject("waypoint_3_3");
            w_3_4 = problem.getObject("waypoint_3_4");
            w_4_0 = problem.getObject("waypoint_4_0");
            w_4_1 = problem.getObject("waypoint_4_1");
            w_4_2 = problem.getObject("waypoint_4_2");
            w_4_3 = problem.getObject("waypoint_4_3");
            w_4_4 = problem.getObject("waypoint_4_4");

        }

        @SuppressWarnings("unchecked")
        @Override
        public List<Statement> createInitStatements() {
            List<Statement> initStatements = new ArrayList<>();
            PersistenceStatement<?> v0_at_0_0 = AT.persistenceStatement(v0).assign(w_0_0);
            initStatements.add(v0_at_0_0);

            PersistenceStatement<?> v1_at_0_0 = AT.persistenceStatement(v1).assign(w_0_0);
            initStatements.add(v1_at_0_0);
            initStatements.add(STATUS.persistenceStatement(v0).assign(landed));
            initStatements.add(STATUS.persistenceStatement(v1).assign(landed));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_0, w_1_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_0, w_0_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_1, w_0_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_1, w_1_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_1, w_0_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_2, w_0_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_2, w_1_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_2, w_0_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_3, w_0_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_3, w_1_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_3, w_0_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_4, w_0_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_0_4, w_1_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_1, w_0_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_1, w_2_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_1, w_1_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_1, w_1_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_2, w_0_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_2, w_2_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_2, w_1_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_2, w_1_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_3, w_0_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_3, w_2_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_3, w_1_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_3, w_1_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_4, w_0_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_4, w_1_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_1_4, w_2_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_1, w_1_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_1, w_3_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_1, w_2_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_1, w_2_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_2, w_1_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_2, w_3_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_2, w_2_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_2, w_2_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_3, w_1_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_3, w_3_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_3, w_2_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_3, w_2_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_4, w_1_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_4, w_2_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_2_4, w_3_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_1, w_2_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_1, w_4_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_1, w_3_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_1, w_3_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_2, w_2_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_2, w_4_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_2, w_3_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_2, w_3_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_3, w_2_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_3, w_4_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_3, w_3_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_3, w_3_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_4, w_2_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_4, w_3_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_3_4, w_4_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_0, w_3_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_0, w_4_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_1, w_3_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_1, w_4_0));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_1, w_4_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_2, w_3_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_2, w_4_1));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_2, w_4_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_3, w_3_3));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_3, w_4_2));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_3, w_4_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_4, w_3_4));
            initStatements.add(CAN_TRAVERSE.persistenceStatement(w_4_4, w_4_3));


            initStatements.add(PHENOMENON_AT.persistenceStatement(w_3_1));
            initStatements.add(PHENOMENON_AT.persistenceStatement(w_2_4));

            for(Statement s : initStatements) {
                //initial time of the world is assumed to be at [-INF, -INF]
                s.estimatedEndEqualsStart();
            }
            return initStatements;
        }
    }

}
