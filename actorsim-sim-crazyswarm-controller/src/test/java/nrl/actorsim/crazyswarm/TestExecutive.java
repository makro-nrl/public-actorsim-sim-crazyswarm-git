package nrl.actorsim.crazyswarm;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import nrl.actorsim.crazyswarm.TestExecutiveHelper.Options;
import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goals.ExecuteStatementGoal;
import nrl.actorsim.planner.PDDLPlanRequestWithMemory;
import nrl.actorsim.planner.PlannerWorkerBase;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static nrl.actorsim.crazyswarm.PlanningDomain.*;
import static nrl.actorsim.domain.ExecuteStatement.State.COMPLETED;
import static nrl.actorsim.goalrefinement.lifecycle.GoalMode.*;
import static nrl.actorsim.goalrefinement.lifecycle.StrategyName.*;
import static org.junit.Assert.assertEquals;

public class TestExecutive {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(TestExecutive.class);

    public static void enableDebugLogging() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger("nrl.actorsim");
        rootLogger.setLevel(Level.DEBUG);
    }

    @BeforeMethod
    public void reset() {
        enableDebugLogging();
        Executive.reset();
    }

    @Test
    public void testLoadDomain() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        Collection<Operator> operators = t.domain.getOperators();
        Assert.assertEquals(operators.size(),4);
        Operator op = t.domain.getOperator("navigate");

        Collection<Statement> conditions = op.getPreconditions();
        assert conditions.size() == 3;

        Collection<Statement> effects = op.getEffects();
        assert effects.size() == 6;
    }

    @Test
    public void testActionInstance() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        for (Operator operator : t.domain.getOperators()) {
            Action action = operator.instance();
            assert action.getPreconditions().size() == operator.getPreconditions().size();
            assert action.getEffects().size() == operator.getEffects().size();
        }
    }

    @Test
    public void testConfirmWaypointInMemory() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");

        WorldObject w_1_1 = WAYPOINT.instance("waypoint_1_1");
        t.memory.assertMissing(w_1_1);

        double simulationTime = 1;
        t.memory.updateOrCloneThenAdd(w_1_1, simulationTime);
        t.memory.assertContains(w_1_1);
    }

    @Test
    public void testInsertPhenomenon() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");

        WorldObject w_1_1 = WAYPOINT.instance("waypoint_1_1");
        t.memory.assertMissing(w_1_1);

        double simulationTime = 1;
        Statement statement = PHENOMENON_AT.persistenceStatement(w_1_1);
        t.memory.updateOrCloneThenAdd(statement, simulationTime);
        t.executive.notifyWorkerAndWaitOnCycle();
        t.memory.assertContains(statement);
    }

    @Test
    public void testInsertInitialState() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.initObjectsAndNotify();

        Duration timeout = Duration.ofSeconds(5);
        t.executive.notifyWorkerAndWaitOnCycle(timeout);

        for (Statement statement : t.init.createInitStatements()) {
            t.memory.assertContains(statement);
        }
    }

    @Test
    public void testBasicFormulateStrategy() {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.strategies.createInspectedStrategies(FORMULATE);
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        PlanningProblem problem = t.domain.getProblem();
        WorldObject w_1_1 = problem.getObject("waypoint_1_1");
        Statement phenomenonAt = PHENOMENON_AT.persistenceStatement(w_1_1);
        t.executive.assertContainsAfterStateUpdate(phenomenonAt);
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal tmpGoal = Goal.createInspectedGoal(w_1_1);
        t.goalMemory.assertContains(tmpGoal);
        assert t.goalMemory.getNumberOfGoals() == 1;
        t.goalMemory.assertModeEquals(tmpGoal, FORMULATED);
    }

    @Test
    public void testInitialPlanIsValid() throws IOException, InterruptedException {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.initObjectsAndNotify();
        WorldObject v1 = t.problem.getObject("v1");
        WorldObject w_0_0 = t.problem.getObject("waypoint_0_0");
        WorldObject w_1_1 = t.problem.getObject("waypoint_1_1");
        WorldObject landedObj = t.problem.getObject("landed");

        t.init.insertAndSelect(Goal.createInspectedGoal(w_1_1));
        //noinspection unchecked
        Goal landed = Goal.createAchievementGoal(STATUS.persistenceStatement(v1).assign(landedObj));
        t.init.insertAndSelect(landed);
        //noinspection unchecked
        Goal atBase = Goal.createAchievementGoal(AT.persistenceStatement(v1).assign(w_0_0));
        t.init.insertAndSelect(atBase);
        t.executive.notifyWorkerAndWaitOnCycle(); //update memory with state

        Goal tmpGoal = Goal.createInspectedGoal(w_1_1);
        t.goalMemory.assertModeEquals(tmpGoal, SELECTED);

        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        PDDLPlanRequestWithMemory request = new PDDLPlanRequestWithMemory(t.memory, t.domain);
        request.readDomain(t.domain.getPddlOptions().domainPath);
        request.generateProblem();
        request.enableCapturePlannerOutput();
        PlannerWorkerBase planner = t.executive.getPlanner();
        planner.enqueue(request);
        planner.notifyWorkerAndWaitOnCycle();
        planner.waitUntilQueueEmpty();
        List<String> plan = request.getPlan();
        assert plan.size() == 8;
    }

    @Test
    public void testCommitCorrectlyCreatesExecutionStatements() throws InterruptedException {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();
        t.executive.getPlanner().getPlannerOptions().plannerTimeoutInSeconds.setValue(300);

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createInspectedStrategies(FORMULATE, SELECT, EXPAND, COMMIT);
        t.strategies.addStrategies(t.memory);
        t.executive.notifyWorkerAndWaitOnCycle();

        WorldObject w_1_1 = t.problem.getObject("waypoint_1_1");
        Goal tmpGoal = Goal.createInspectedGoal(w_1_1);
        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == COMMITTED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assertEquals(COMMITTED, goalInMemory.getMode());
        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assertEquals(7, children.length);
        assertEquals(10, t.goalMemory.getNumberOfGoals());
    }

    @Test
    public void testExecutionSucceedsWithoutExecutive() throws InterruptedException {
/* =================================================================
   =================================================================
   TODO fix this test failing if run as part of the entire class but succeeding when run alone
        Most likely this is just an initialization problem with the setup()
   =================================================================
   =================================================================
 */
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createAllInspectedStrategies();
        t.strategies.addAllExecuteStatementStrategies();
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        WorldObject w_1_1 = t.problem.getObject("waypoint_1_1");
        Goal tmpGoal = Goal.createInspectedGoal(w_1_1);

        logger.debug(t.goalMemory.toString());
        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == DISPATCHED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == DISPATCHED;

        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 7;
        assert t.goalMemory.getNumberOfGoals() == 10;

        Duration timeout = Duration.ofSeconds(10);
        Instant start = Instant.now();

        while (children.length > 0) {
            logger.debug(t.goalMemory.toString());
            for (GoalLifecycleNode node : children) {
                if (node.modeMatches(DISPATCHED)
                        && node.isNotComplete() ) {
                    if (node instanceof ExecuteStatementGoal) {
                        //complete the first one found that is dispatched and incomplete
                        ExecuteStatement statement = node.content();
                        statement.attemptTransitionToAndNotify(COMPLETED);
                        Action action = statement.getAction();
                        logger.debug("Transition to COMPLETED action '{}' with step {}", action.getLabel(), statement.getPlanStep());
                    }
                    break;
                }
            }
            Thread.sleep(1000);
            children = goalInMemory.getSubgoals();
            Duration elapsed = Duration.between(Instant.now(), start);
            assert elapsed.compareTo(timeout) < 0;
        }
        children = goalInMemory.getSubgoals();
        assert children.length == 0;
    }

    @Test(enabled = false)
    public void testExecutionSucceedsWithExecutive() throws InterruptedException, IOException {
        TestExecutiveHelper t = new TestExecutiveHelper("vehicle-test");
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createAllInspectedStrategies();
        t.strategies.addAllExecuteStatementStrategies();
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        WorldObject w_1_1 = t.problem.getObject("waypoint_1_1");
        Goal tmpGoal = Goal.createInspectedGoal(w_1_1);

        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
            return goalInMemory.getMode() == DISPATCHED;
        });
        t.executive.notifyWorkerAndWaitOnCycle();
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(tmpGoal);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == DISPATCHED;

        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 7;

        Duration timeout = Duration.ofSeconds(children.length * 3);
        Instant start = Instant.now();

        //noinspection unused
        CrazySwarmClient client = new CrazySwarmClient(t.executive);

        while (children.length > 0) {
            logger.info("Waiting for CrazySwarmClient to send commands");
            Thread.sleep(5000);
            children = goalInMemory.getSubgoals();
            Duration elapsed = Duration.between(Instant.now(), start);
            assert elapsed.compareTo(timeout) < 0;
        }
        children = goalInMemory.getSubgoals();
        assert children.length == 0;
    }

    @Test(enabled = false)
    public void testExecutionSucceedsWithExecutive_3_vehicles() throws InterruptedException, IOException {
        Options options = new Options("vehicle-test");
        options.numVehicles = 3;
        options.GRID_SIZE = 5;
        options.phenomena = new String[] { "waypoint_0_5", "waypoint_5_0", "waypoint_3_2" };
        TestExecutiveHelper t = new TestExecutiveHelper(options);
        t.executive.getPlanner().getPlannerOptions().planForAllGoalsInMemory();
        t.initObjectsAndNotify();

        Goal.addReturnToBaseGoals(t.executive);
        t.executive.notifyWorkerAndWaitOnCycle();

        t.strategies.createAllInspectedStrategies();
        t.strategies.addAllExecuteStatementStrategies();
        t.strategies.addStrategies(t.memory);
        t.initObjectsAndNotify();

        WorldObject w_3_2 = t.problem.getObject("waypoint_3_2");
        Goal inspect_3_2 = Goal.createInspectedGoal(w_3_2);

        t.executive.waitUntil(Duration.ofSeconds(5), node -> {
            Goal goalInMemory = t.goalMemory.findFirst(inspect_3_2);
            if (goalInMemory != null) {
                return goalInMemory.getMode() == DISPATCHED;
            }
            return false;
        });
        t.executive.notifyWorkerAndWaitOnCycle();
        t.executive.notifyWorkerAndWaitOnCycle();

        Goal goalInMemory = t.goalMemory.findFirst(inspect_3_2);
        logger.debug(t.goalMemory.toString());

        assert goalInMemory.getMode() == DISPATCHED;

        GoalLifecycleNode[] children = goalInMemory.getSubgoals();
        assert children.length == 39;

        Duration timeout = Duration.ofSeconds(children.length * 3);
        Instant start = Instant.now();

        //noinspection unused
        CrazySwarmClient client = new CrazySwarmClient(t.executive);

        while (children.length > 0) {
            logger.info("Waiting for CrazySwarmClient to send commands");
            Thread.sleep(5000);
            children = goalInMemory.getSubgoals();
            Duration elapsed = Duration.between(Instant.now(), start);
            assert elapsed.compareTo(timeout) < 0;
        }
        children = goalInMemory.getSubgoals();
        assert children.length == 0;
    }

}
