package nrl.actorsim.crazyswarm;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import nrl.actorsim.domain.*;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.*;

import static nrl.actorsim.crazyswarm.TestExecutiveHelper.*;

public class TestUxVExecutive {

    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestUxVExecutive.class);

    public static void enableDebugLogging() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger("nrl.actorsim");
        rootLogger.setLevel(Level.DEBUG);
    }

    @BeforeMethod
    public void reset() {
        enableDebugLogging();
        Executive.reset();
    }

    @Test
    public void testLoadUUVDomain() {
        PlanningDomain domain = loadDomain("uuv-test");
        PlanningProblem problem = domain.getProblem();
        Collection<Operator> operators = domain.getOperators();
        Assert.assertEquals(operators.size(),6);
        Operator op = domain.getOperator("navigate");

        Collection<Statement> conditions = op.getPreconditions();
        assert conditions.size() == 4;

        Collection<Statement> effects = op.getEffects();
        assert effects.size() == 2;
    }

    @Test
    public void testLoadUAVDomain() {
        PlanningDomain domain = loadDomain("uav-test");
        PlanningProblem problem = domain.getProblem();
        Collection<Operator> operators = domain.getOperators();
        Assert.assertEquals(operators.size(),5);
        Operator op = domain.getOperator("takeoff");

        Collection<Statement> conditions = op.getPreconditions();
        assert conditions.size() == 3;

        Collection<Statement> effects = op.getEffects();
        assert effects.size() == 3;
    }

    @Test
    public void testActionInstance() {
        PlanningDomain domain = loadDomain("uuv-test");
        PlanningProblem problem = domain.getProblem();
        for (Operator operator : domain.getOperators()) {
            Action action = operator.instance();
            assert action.getPreconditions().size() == operator.getPreconditions().size();
            assert action.getEffects().size() == operator.getEffects().size();
        }
    }


}
