#/usr/bin/bash

server_home=../usc-server-code-git
scripts_dir=$server_home/ros_ws/src/crazyswarm/scripts
launch_dir=$server_home/ros_ws/src/crazyswarm/launch
server_module_dir=actorsim-sim-crazyswarm-server/src/main/python

if [ ! -d $server_home ]
then
    echo "The directory '$server_home' must exist"
    echo "It needs to be created using the command:"
    echo "git clone https://github.com/USC-ACTLab/crazyswarm.git usc-server-code-git"
    exit
fi


echo "Server directory exists: '$server_home'"
set -x
cp -r $scripts_dir/pycrazyswarm $server_module_dir
cp $scripts_dir/uav_trajectory.py $server_module_dir
cp $launch_dir/crazyflies.yaml $server_module_dir




