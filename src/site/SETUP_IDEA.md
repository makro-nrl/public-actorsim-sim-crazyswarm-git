# Installing Malmo Connector with IntelliJ IDEA

**Some steps in this section are interdependent so please
read entirely through the rest of this document before continuing!**


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Install IntelliJ IDEA
The core module has detailed instructions for installing IntelliJ
and the recommended base development environment for ActorSim the first time.
- The public version of this document is available at
  [this link](https://bitbucket.org/makro-nrl2/actorsim-core/src/develop/src/site/INSTALL_IDEA.md).  
- If you are viewing this file on your local file system in a web browser, 
  then you can follow [this link](../../INSTALL_IDEA.md).
- If you are viewing this file within IDEA, you can try 
  [this link](../../../actorsim-core-git/src/site/INSTALL_IDEA.md). 
- Otherwise, you will need to navigate to the file 
  at ACTORSIM_CORE/src/site/INSTALL_IDEA.md.

Once you have installed IntelliJ IDEA, return to this point.

**Note: You must install the python plugin!**



[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Setup IntelliJ IDEA by importing a project
We've created a project template for you to import.

### Set up the Path variables for the project.
1. Open the IDEA Global Settings in one of two ways
   - If you already have another project open, then open `File > Settings` and search for `variables`  
   - If you are at the project management screen, select `Configure > Settings`
1. Add CRAZYSWARM_ACTORSIM_CORE and select the CS_ACTORSIM_CORE path
1. Add CRAZYSWARM_SIM and select the CS_HOME path
1. Add CRAZYSWARM_POPF and select the CS_POPF path
1. Add CRAZYSWARM_SERVER and select the CS_SERVER path
   ![Setup Path Variables](_images/IDEA_CS_PathVariables.png)

### Open the project

1. Select `Open` 
1. Navigate to`CS_HOME/idea-projects/crazyswarm-project` and click `OK`
   ![Open Project](_images/IDEA_CS_OpenProject.png)

### Reimport the maven project
1. Select the Maven tab on the right side of your window 
   or open from the menu `View > Tool Windows > Maven`
1. Click the 'Reload All Maven Projects' button in the top toolbar 
   ![Reimport Maven Projects](_images/IDEA_CS_MavenImport.png)

The resulting project structure -- in the tab all the way to the left -- should 
be similar to the following image, except your branch names should
all be `develop`. Note the bold modules.

   ![Expected Project Structure](_images/IDEA_CS_ProjectStructure.png)


Return to [INSTALL.md](INSTALL.md) and continue.
