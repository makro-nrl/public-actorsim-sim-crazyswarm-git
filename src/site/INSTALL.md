# The CrazySwarm Connector Environment

The CrazySwarm simulator has several components: 
  - the python simulator (**cs-server**) that runs the simulation, 
  - the python client (**cs-controller**) that connects to the server, 
  - the POPF planner (**popf-planner** that generates temporal plans, and 
  - the java executive (**cs-executive**) that manages the goals.

This guide will help you get the above modules installed and running in IntelliJ. 

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Prerequisites and Platforms

The steps to set up this connector for ActorSim involve:
- Installing ActorSim's common prerequisites for this connector, which include:
  - (Optional) a supercharged shell environment
  - git
  - Java OpenJDK 11.0
  - Python 2.7
  - Python's virtual environment
- Configuring a virtual environment in which to run CrazySwarm
- Installing and testing CrazySwarm on the command line
- Cloning the ActorSim source directories on the command line
- Building and testing the server and client on the command line
- Installing the IntelliJ IDEA, into which you will also install and test:
  - The (optional) CrazySwarm server module
  - The CrazySwarm Python Controller module
  - The ActorSim CrazySwarm Executive module
  - The POPF planning connector module
- Setting up an IDEA MultiRun configuration to run the tests for a full
  working example of ActorSim, POPF, and CrazySwarm

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Install ActorSim's Common Prerequisites

The core module has detailed instructions for installing 
the recommended base development environment.
- The public version of this document is available at:
  [this link](https://bitbucket.org/makro-nrl2/actorsim-core/src/develop/src/site/INSTALL_common_prereqs.md).  
- If you are viewing this file on a web browser, then you can follow
  [this link](../../INSTALL_common_prereqs.md).
- If you are viewing this file within IDEA, you can try 
  [this link](../../../actorsim-core-git/src/site/INSTALL_common_prereqs.md). 
- Otherwise, you will need to navigate to the file 
  at ACTORSIM_CORE/src/site/INSTALL_common_prereqs.md.

Once you have installed the common requirements, return to this point.

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
### Create your base project directory

To simplify and unify our development environments across many projects
 **please** clone your repos into these directories and names.  
 
Let CS_BASE_DIR represent the root directory for this project.
This directory can be anywhere you like, but keep in mind that you may want
to have a short path because you will be required to type _absolute_ paths
 during the IDE setup.  Shown below are the recommended paths.
 
![Recommended Directory Structure](_images/RecommendedDirectoryStructure.png)

which equate to the following table: 

|Required | VARIABLE NAME     | Local Directory           | Description  |
|:------- | :------------- |:------------- | :-----|
| Y| CS_BASE_DIR          | `~/git-workspace/crazyswarm`  | Your base install directory |
| Y| CS_HOME   | `CS_BASE_DIR/actorsim-sim-crazyswarm-git` | The CrazySwarm connector code |
| Y| CS_ACTORSIM_HOME | `CS_BASE_DIR/actorsim-core-git` | ActorSim's core goal refinement library |
| N (Optional!)| CS_SERVER | `CS_BASE_DIR/crazyswarm-git` | The CrazySwarm server library (optional!)|
Henceforth, we will refer to these directories by the variable name and
 you will need to replace them as appropriate.

These are not shell variables, so commands below are representative
of the commands you will type. 

Set up a common workspace for this connector
```shell script
cd ~
mkdir -p CS_BASE_DIR
```


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Create a virtualenv and test the server 

We have provided a prebuilt binary of the CrazySwarm code 
for the Linux x64 architecture, but you still need to 
create a virtual environment and install python dependencies
into that virtual environment.
```
cd ~/virtualenv #create if needed
virtualenv -p python2.7 crazyswarm
source ~/virtualenv/crazyswarm/bin/activate  #or 'workon crazyswarm' if you have mak's zsh setup
pip install numpy pyyaml sphinx sphinx_rtd_theme matplotlib  # crazyswarm prerequisites 
pip install enum34  # server prerequisite
sudo apt install python-tk  #required for visualization
```


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Make sure it works
```
cd CS_HOME/actorsim-sim-crazyswarm-server/src/main/python
python figure8_csv.py --sim
```
You should see a window appear with a dot that moves in a figure 8.
![Image of Figure8 example running](_images/CrazySwarm_figure8.png)


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Run the server and client

You also need to install a python client for your virtual environment:
```shell script
source ~/virtualenv/crazyswarm/bin/activate  #or 'workon crazyswarm' if you have mak's zsh setup
pip install python-memcached
```

Now you will need to open three shell windows:
- In the first window, start memcached:
```shell script
memcached -vv
```

- In the second window, start the server:
  (you can either run `CS/run-server.sh` after activating the virtualenv or do the following)
```shell script
source ~/virtualenv/crazyswarm/bin/activate  #or 'workon crazyswarm' if you have mak's zsh setup
cd CS_HOME/actorsim-sim-crazyswarm-server/src/main/python
python server.py
```


- In the final window, run the test:
  (you can either run `CS/run-test-client.sh` after activating the virtualenv or do the following)
```shell script
source ~/virtualenv/crazyswarm/bin/activate  #or 'workon crazyswarm' if you have mak's zsh setup
cd CS_HOME/actorsim-sim-crazyswarm-server/src/test/python
export PYTHONPATH=../../main/python/
python TestCrazySwarmServer.py
```

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Setup the IntelliJ IDE
Now that the server and test client work, you are ready to set up the IDE.

IntelliJ IDEA with a python plugin that allows for running
java and python code simultaneously.  
See [SETUP_IDEA.md](SETUP_IDEA.md).



[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Set up the python environment to run the Server example

To confirm that your IntelliJ setup is correct, we will run the same test
from above that you did at the command line.

Inside of IDEA, open the CrazySwarm server module
(**actorsim-sim-crazyswarm-server**) and select the file
`src/test/python/TestCrazySwarmServer.py`.

When you open it the first time, you will likely see errors in the
file because you have not yet connected a python environment
to this module.  

Open the project structure (`File > Project Structure`) and:
1. Select Modules
1. Select the module `actorsim-sim-crazyswarm-server`
1. Select Dependencies
1. Select the drop down for the Module SDK
1. If the Python 2.7 Crazyswarm virtual environment is available select it.
   Otherwise, create a new SDK by doing the following:
   - Select `Add SDK > Python SDK`
   - Select virtuenv environment
   - Select `Use existing environment`
   - Navigate to the directory `~/virtualenv/crazyswarm/bin/python`
     ![Adding a new Python SDK](_images/IDEA_CS_NewPythonSDK.png)
   
When you are finished, it should appear:
 - ![Using the Crazyswarm Virtualenv](_images/IDEA_CS_PythonSDK.png)
   
Run the test:
  - Run memcached in its own terminal as described above.
  - Run the server in its own terminal as described above.
  - Click the green "play button" next to the `test_square()` method
    and select `Debug unittests ...`
  - You should see a similar window appear as above and the agent moving around.


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Test the CrazySwarm Java client that also runs ActorSim code

Inside of the IDEA, open the controller module **actorsim-sim-crazyswarm-controller**
navigate to `src/test/java/` and select the file `nrl.actorsim.crazyswarm.TestExecutive`.

Run the test:
  - Run memcached in its own terminal as described above.
  - Run the server in its own terminal as described above.
  - Click the green "play button" next to the entire class 
    and select `Debug TestVehicleExecutive...`
  - You should see all tests passing. 
    If one or more tests fail, try to run it by itself to determine the 
    cause and report the error on the ActorSim-Devs Teams channel for CrazySwarm.
 



[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## (Optional) Install the original CrazySwarm library
This step is *not* needed to run the demo.
It is only needed if you wish to develop against the CrazySwarm library.
CrazySwarm allows you to install via Conda or build it from scratch.
We generally recommend building from scratch to match other 
connectors in ActorSim and so the team can help isolate problems.
If you prefer to use Conda, see the documents for CrazySwarm: 
https://crazyswarm.readthedocs.io/

Create a virtual environment and install python deps.
**Skip this step if you have already done above.**
```
cd ~/virtualenv #create if needed
virtualenv -p python2.7 crazyswarm
source ~/virtualenv/crazyswarm/bin/activate  #or 'workon crazyswarm' if you have mak's zsh setup
pip install numpy vispy pyyaml sphinx sphinx_rtd_theme 
```


### Clone the CrazySwarm server
```
cd <CS_BASE_DIR>
git clone https://github.com/USC-ACTLab/crazyswarm.git crazyswarm-server-git  
```

### Build the CrazySwarm server
```
sudo apt install git make gcc swig libpython-dev python-numpy python-yaml python-matplotlib
cd crazyswarm-server-git
./buildSimOnly.sh
```

### Make sure it works
```
cd ros_ws/src/crazyswarm/scripts
$ python figure8_csv.py --sim
```
You should see a window appear with a dot that moves in a figure 8.
![Image of Figure8 example running](_images/CrazySwarm_figure8.png)

