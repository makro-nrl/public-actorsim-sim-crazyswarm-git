;;  ---------- Start of domain debug
;;  ---------- Loaded from file: /home/vivint/git/crazyswarm_workspace/crazyswarm/actorsim-sim-crazyswarm/idea-projects/project-crazyswarm/src/main/resources/vehicle-test-domain.pddl
;;  ---------- End of domain debug
(define
  (problem task)
  (:domain vehicle)

  (:objects
    flying - STAT
    hover - STAT
    landed - STAT
    sensing - STAT
    v1 - VEHICLE
    v2 - VEHICLE
    v3 - VEHICLE
    waypoint_0_0 - WAYPOINT
    waypoint_0_1 - WAYPOINT
    waypoint_0_2 - WAYPOINT
    waypoint_0_3 - WAYPOINT
    waypoint_0_4 - WAYPOINT
    waypoint_0_5 - WAYPOINT
    waypoint_0_6 - WAYPOINT
    waypoint_1_0 - WAYPOINT
    waypoint_1_1 - WAYPOINT
    waypoint_1_2 - WAYPOINT
    waypoint_1_3 - WAYPOINT
    waypoint_1_4 - WAYPOINT
    waypoint_1_5 - WAYPOINT
    waypoint_1_6 - WAYPOINT
    waypoint_2_0 - WAYPOINT
    waypoint_2_1 - WAYPOINT
    waypoint_2_2 - WAYPOINT
    waypoint_2_3 - WAYPOINT
    waypoint_2_4 - WAYPOINT
    waypoint_2_5 - WAYPOINT
    waypoint_2_6 - WAYPOINT
    waypoint_3_0 - WAYPOINT
    waypoint_3_1 - WAYPOINT
    waypoint_3_2 - WAYPOINT
    waypoint_3_3 - WAYPOINT
    waypoint_3_4 - WAYPOINT
    waypoint_3_5 - WAYPOINT
    waypoint_3_6 - WAYPOINT
    waypoint_4_0 - WAYPOINT
    waypoint_4_1 - WAYPOINT
    waypoint_4_2 - WAYPOINT
    waypoint_4_3 - WAYPOINT
    waypoint_4_4 - WAYPOINT
    waypoint_4_5 - WAYPOINT
    waypoint_4_6 - WAYPOINT
    waypoint_5_0 - WAYPOINT
    waypoint_5_1 - WAYPOINT
    waypoint_5_2 - WAYPOINT
    waypoint_5_3 - WAYPOINT
    waypoint_5_4 - WAYPOINT
    waypoint_5_5 - WAYPOINT
    waypoint_5_6 - WAYPOINT
    waypoint_6_0 - WAYPOINT
    waypoint_6_1 - WAYPOINT
    waypoint_6_2 - WAYPOINT
    waypoint_6_3 - WAYPOINT
    waypoint_6_4 - WAYPOINT
    waypoint_6_5 - WAYPOINT
    waypoint_6_6 - WAYPOINT
  ) ;objects

  (:init
    (at v1 waypoint_0_0)
    (at v2 waypoint_0_0)
    (at v3 waypoint_0_0)
    (can_traverse waypoint_0_0 waypoint_0_1)
    (can_traverse waypoint_0_0 waypoint_1_0)
    (can_traverse waypoint_0_1 waypoint_0_0)
    (can_traverse waypoint_0_1 waypoint_0_2)
    (can_traverse waypoint_0_1 waypoint_1_1)
    (can_traverse waypoint_0_2 waypoint_0_1)
    (can_traverse waypoint_0_2 waypoint_0_3)
    (can_traverse waypoint_0_2 waypoint_1_2)
    (can_traverse waypoint_0_3 waypoint_0_2)
    (can_traverse waypoint_0_3 waypoint_0_4)
    (can_traverse waypoint_0_3 waypoint_1_3)
    (can_traverse waypoint_0_4 waypoint_0_3)
    (can_traverse waypoint_0_4 waypoint_0_5)
    (can_traverse waypoint_0_4 waypoint_1_4)
    (can_traverse waypoint_0_5 waypoint_0_4)
    (can_traverse waypoint_0_5 waypoint_0_6)
    (can_traverse waypoint_0_5 waypoint_1_5)
    (can_traverse waypoint_0_6 waypoint_0_5)
    (can_traverse waypoint_1_0 waypoint_0_0)
    (can_traverse waypoint_1_0 waypoint_1_1)
    (can_traverse waypoint_1_0 waypoint_2_0)
    (can_traverse waypoint_1_1 waypoint_0_1)
    (can_traverse waypoint_1_1 waypoint_1_0)
    (can_traverse waypoint_1_1 waypoint_1_2)
    (can_traverse waypoint_1_1 waypoint_2_1)
    (can_traverse waypoint_1_2 waypoint_0_2)
    (can_traverse waypoint_1_2 waypoint_1_1)
    (can_traverse waypoint_1_2 waypoint_1_3)
    (can_traverse waypoint_1_2 waypoint_2_2)
    (can_traverse waypoint_1_3 waypoint_0_3)
    (can_traverse waypoint_1_3 waypoint_1_2)
    (can_traverse waypoint_1_3 waypoint_1_4)
    (can_traverse waypoint_1_3 waypoint_2_3)
    (can_traverse waypoint_1_4 waypoint_0_4)
    (can_traverse waypoint_1_4 waypoint_1_3)
    (can_traverse waypoint_1_4 waypoint_1_5)
    (can_traverse waypoint_1_4 waypoint_2_4)
    (can_traverse waypoint_1_5 waypoint_0_5)
    (can_traverse waypoint_1_5 waypoint_1_4)
    (can_traverse waypoint_1_5 waypoint_1_6)
    (can_traverse waypoint_1_5 waypoint_2_5)
    (can_traverse waypoint_1_6 waypoint_1_5)
    (can_traverse waypoint_2_0 waypoint_1_0)
    (can_traverse waypoint_2_0 waypoint_2_1)
    (can_traverse waypoint_2_0 waypoint_3_0)
    (can_traverse waypoint_2_1 waypoint_1_1)
    (can_traverse waypoint_2_1 waypoint_2_0)
    (can_traverse waypoint_2_1 waypoint_2_2)
    (can_traverse waypoint_2_1 waypoint_3_1)
    (can_traverse waypoint_2_2 waypoint_1_2)
    (can_traverse waypoint_2_2 waypoint_2_1)
    (can_traverse waypoint_2_2 waypoint_2_3)
    (can_traverse waypoint_2_2 waypoint_3_2)
    (can_traverse waypoint_2_3 waypoint_1_3)
    (can_traverse waypoint_2_3 waypoint_2_2)
    (can_traverse waypoint_2_3 waypoint_2_4)
    (can_traverse waypoint_2_3 waypoint_3_3)
    (can_traverse waypoint_2_4 waypoint_1_4)
    (can_traverse waypoint_2_4 waypoint_2_3)
    (can_traverse waypoint_2_4 waypoint_2_5)
    (can_traverse waypoint_2_4 waypoint_3_4)
    (can_traverse waypoint_2_5 waypoint_1_5)
    (can_traverse waypoint_2_5 waypoint_2_4)
    (can_traverse waypoint_2_5 waypoint_2_6)
    (can_traverse waypoint_2_5 waypoint_3_5)
    (can_traverse waypoint_2_6 waypoint_2_5)
    (can_traverse waypoint_3_0 waypoint_2_0)
    (can_traverse waypoint_3_0 waypoint_3_1)
    (can_traverse waypoint_3_0 waypoint_4_0)
    (can_traverse waypoint_3_1 waypoint_2_1)
    (can_traverse waypoint_3_1 waypoint_3_0)
    (can_traverse waypoint_3_1 waypoint_3_2)
    (can_traverse waypoint_3_1 waypoint_4_1)
    (can_traverse waypoint_3_2 waypoint_2_2)
    (can_traverse waypoint_3_2 waypoint_3_1)
    (can_traverse waypoint_3_2 waypoint_3_3)
    (can_traverse waypoint_3_2 waypoint_4_2)
    (can_traverse waypoint_3_3 waypoint_2_3)
    (can_traverse waypoint_3_3 waypoint_3_2)
    (can_traverse waypoint_3_3 waypoint_3_4)
    (can_traverse waypoint_3_3 waypoint_4_3)
    (can_traverse waypoint_3_4 waypoint_2_4)
    (can_traverse waypoint_3_4 waypoint_3_3)
    (can_traverse waypoint_3_4 waypoint_3_5)
    (can_traverse waypoint_3_4 waypoint_4_4)
    (can_traverse waypoint_3_5 waypoint_2_5)
    (can_traverse waypoint_3_5 waypoint_3_4)
    (can_traverse waypoint_3_5 waypoint_3_6)
    (can_traverse waypoint_3_5 waypoint_4_5)
    (can_traverse waypoint_3_6 waypoint_3_5)
    (can_traverse waypoint_4_0 waypoint_3_0)
    (can_traverse waypoint_4_0 waypoint_4_1)
    (can_traverse waypoint_4_0 waypoint_5_0)
    (can_traverse waypoint_4_1 waypoint_3_1)
    (can_traverse waypoint_4_1 waypoint_4_0)
    (can_traverse waypoint_4_1 waypoint_4_2)
    (can_traverse waypoint_4_1 waypoint_5_1)
    (can_traverse waypoint_4_2 waypoint_3_2)
    (can_traverse waypoint_4_2 waypoint_4_1)
    (can_traverse waypoint_4_2 waypoint_4_3)
    (can_traverse waypoint_4_2 waypoint_5_2)
    (can_traverse waypoint_4_3 waypoint_3_3)
    (can_traverse waypoint_4_3 waypoint_4_2)
    (can_traverse waypoint_4_3 waypoint_4_4)
    (can_traverse waypoint_4_3 waypoint_5_3)
    (can_traverse waypoint_4_4 waypoint_3_4)
    (can_traverse waypoint_4_4 waypoint_4_3)
    (can_traverse waypoint_4_4 waypoint_4_5)
    (can_traverse waypoint_4_4 waypoint_5_4)
    (can_traverse waypoint_4_5 waypoint_3_5)
    (can_traverse waypoint_4_5 waypoint_4_4)
    (can_traverse waypoint_4_5 waypoint_4_6)
    (can_traverse waypoint_4_5 waypoint_5_5)
    (can_traverse waypoint_4_6 waypoint_4_5)
    (can_traverse waypoint_5_0 waypoint_4_0)
    (can_traverse waypoint_5_0 waypoint_5_1)
    (can_traverse waypoint_5_0 waypoint_6_0)
    (can_traverse waypoint_5_1 waypoint_4_1)
    (can_traverse waypoint_5_1 waypoint_5_0)
    (can_traverse waypoint_5_1 waypoint_5_2)
    (can_traverse waypoint_5_1 waypoint_6_1)
    (can_traverse waypoint_5_2 waypoint_4_2)
    (can_traverse waypoint_5_2 waypoint_5_1)
    (can_traverse waypoint_5_2 waypoint_5_3)
    (can_traverse waypoint_5_2 waypoint_6_2)
    (can_traverse waypoint_5_3 waypoint_4_3)
    (can_traverse waypoint_5_3 waypoint_5_2)
    (can_traverse waypoint_5_3 waypoint_5_4)
    (can_traverse waypoint_5_3 waypoint_6_3)
    (can_traverse waypoint_5_4 waypoint_4_4)
    (can_traverse waypoint_5_4 waypoint_5_3)
    (can_traverse waypoint_5_4 waypoint_5_5)
    (can_traverse waypoint_5_4 waypoint_6_4)
    (can_traverse waypoint_5_5 waypoint_4_5)
    (can_traverse waypoint_5_5 waypoint_5_4)
    (can_traverse waypoint_5_5 waypoint_5_6)
    (can_traverse waypoint_5_5 waypoint_6_5)
    (can_traverse waypoint_5_6 waypoint_5_5)
    (can_traverse waypoint_5_6 waypoint_6_6)
    (can_traverse waypoint_6_0 waypoint_5_0)
    (can_traverse waypoint_6_1 waypoint_5_1)
    (can_traverse waypoint_6_2 waypoint_5_2)
    (can_traverse waypoint_6_3 waypoint_5_3)
    (can_traverse waypoint_6_4 waypoint_5_4)
    (can_traverse waypoint_6_5 waypoint_5_5)
    (can_traverse waypoint_6_5 waypoint_6_6)
    (can_traverse waypoint_6_6 waypoint_5_6)
    (can_traverse waypoint_6_6 waypoint_6_5)
    (phenomenon_at waypoint_0_5)
    (phenomenon_at waypoint_2_2)
    (phenomenon_at waypoint_5_0)
    (status v1 landed)
    (status v2 landed)
    (status v3 landed)
  ) ;init

  (:goal 
    (and
      (at v1 waypoint_0_0)
      (at v2 waypoint_0_0)
      (at v3 waypoint_0_0)
      (inspected waypoint_0_5)
      (inspected waypoint_2_2)
      (inspected waypoint_5_0)
      (status v1 landed)
      (status v2 landed)
      (status v3 landed)
    ) ;and
  ) ;goal

) ;define
