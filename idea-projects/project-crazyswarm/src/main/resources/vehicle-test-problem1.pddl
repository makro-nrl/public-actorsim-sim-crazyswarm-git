(define (problem vehicle-simple) (:domain vehicle)
(:objects
	v0 - vehicle
	waypoint_0_0 waypoint_0_1 waypoint_1_0 waypoint_1_1 - Waypoint
)

(:init
  (at v0 waypoint_0_0)
  (status v0 LANDED)

  (can_traverse waypoint_0_0 waypoint_0_1)
  (can_traverse waypoint_0_1 waypoint_0_0)

  (can_traverse waypoint_0_0 waypoint_1_0)
  (can_traverse waypoint_1_0 waypoint_0_0)

  (can_traverse waypoint_1_0 waypoint_1_1)
  (can_traverse waypoint_1_1 waypoint_1_0)

  (can_traverse waypoint_0_1 waypoint_1_1)
  (can_traverse waypoint_1_1 waypoint_0_1)

  (phenomenon_at waypoint_1_1)
)

(:goal (and
        (at v0 waypoint_0_0)
        (status v0 LANDED)
        (inspected waypoint_1_1)
	)
)

(:metric minimize (total-time))
)
