(define (problem uavprob001) (:domain UAV)
(:objects
	uav0 - uav
	color high_res low_res - Mode
	x0 x1 - waypointx
	y0 y1 - waypointy
	z0 z1 - waypointz
	)
(:init
	(at uav0 x0 y0 z0)
	(not(power_on uav0))
	(landed uav0)
    (can_fly uav0 x0 y0 z1 x0 y1 z1)
    (can_fly uav0 x0 y0 z1 x1 y0 z1)
    (can_fly uav0 x0 y1 z1 x1 y1 z1)
    (can_fly uav0 x0 y1 z1 x0 y0 z1)
	(can_fly uav0 x1 y1 z1 x1 y0 z1)
    (can_fly uav0 x1 y1 z1 x0 y1 z1)
    (can_fly uav0 x1 y0 z1 x0 y0 z1)
    (can_fly uav0 x1 y0 z1 x1 y1 z1)
    (can_takeoff uav0 x0 y0 z0)
    (can_takeoff uav0 x0 y1 z0)
    (can_takeoff uav0 x1 y1 z0)
    (can_takeoff uav0 x1 y0 z0)
    (can_land uav0 x0 y0 z1)
    (can_land uav0 x0 y1 z1)
    (can_land uav0 x1 y1 z1)
    (can_land uav0 x1 y0 z1)
)

(:goal (and

        (at uav0 x0 y0 z0)
        (visited uav0 x1 y1 z1)

        )
)

(:metric minimize (total-time))
)
